<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('/', function () {
    return view('welcome');
});

Route::resource('/index','indexController');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['namespace'=>'Administrator','prefix'=>'admin'],function (){

    Route::resource('panel','panelController');

    Route::resource('user','userController');

    Route::get('slider','sliderController@show');

    Route::post('save','sliderController@save');

    Route::get('comment','commentController@show');

    Route::patch('agree','commentController@update');

    Route::delete('remove','commentController@remove');

    Route::resource('category','catController');

    Route::resource('new','newController');

    Route::patch('update','newsController@refresh');

    Route::resource('tab','tabController');

    Route::resource('report','repotimageController');

    Route::patch('reupdate','reController@update');

    Route::resource('login','loginController');

    Route::resource('profile','profileController');

    Route::resource('menu','menuController');

    Route::resource('submenu','submenuController');

    Route::PATCH('disable','menuactiveController@disable');

    Route::PATCH('enable','menuactiveController@enable');

    Route::PATCH('enablesub','subactiveController@enable');

    Route::PATCH('disablesub','subactiveController@disable');

    Route::resource('workgroup','workgroupController');

    Route::PATCH('active','workController@active');

    Route::PATCH('deactive','workController@deactive');

    Route::post('premesion','premesionController@store');

    Route::get('getpdf','pdfController@getpdf');

    Route::get("fullcalendar", "Calender@Chartjs");

    Route::resource("connect", "contectusController");

    Route::resource("view","viewController");

});
