<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class contect extends Model
{
    protected $table="contact_us";
    public $timestamps=false;
    protected $fillable=['ID','name','family','email','phone','content'];

}
