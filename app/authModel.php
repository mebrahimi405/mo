<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class authModel extends Model
{
   protected $table='auth_item_child';
   public $timestamps=false;
    protected $fillable=['ID','item_name','child'];

}
