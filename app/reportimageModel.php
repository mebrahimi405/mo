<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class reportimageModel extends Model
{
    protected $table="reportimage";
    protected $fillable=['id','name','date','time','user','picture','status'];
}
