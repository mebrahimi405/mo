<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class loginModel extends Model
{
    protected $table="login_config";
    protected $fillable=['id','title','logo','copyright'];
}
