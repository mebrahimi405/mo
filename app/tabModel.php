<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class tabModel extends Model
{
    protected $table="Advertising";
    protected $fillable=['id','imgpic', 'text'];
}
