<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;


class newModel extends Model
{

    use Sluggable;

    protected $table="news";
    protected $fillable=['id','title','slug','date','time','user','context','image','category','status'];
    public function news(){
        return $this->belongsTo(categoryModel::class);
    }
    public function Sluggable(){
        return[
            'slug'=>[
                'source'=>'title'
            ]
        ];
    }
    public function path(){
        return "/index/$this->slug";
    }
}
