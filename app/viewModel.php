<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class viewModel extends Model
{
    protected $table="view";
    public $timestamps=false;
    protected $fillable=['ID','news_id','view'];
}
