<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class workgroupModel extends Model
{
    protected $table="auth_item_date";
    protected $fillable=['item_name','title','status'];
}
