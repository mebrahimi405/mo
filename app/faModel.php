<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class faModel extends Model
{
    protected $table="fa";
    protected $fillable=['id','name'];
}
