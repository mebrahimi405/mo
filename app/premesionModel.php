<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class premesionModel extends Model
{
    protected $table="auth_assignment";
    public $timestamps = false;
    protected $fillable=['id','item_name','user_id'];
}
