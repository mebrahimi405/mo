<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class workmenuModel extends Model
{

    protected $table="auth_menu";
    protected $fillable=['id','item_name','menu_id'];
}
