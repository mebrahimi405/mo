<?php

namespace App\Http\Controllers\Administrator;

use App\Http\Controllers\Controller;
use App\Http\Requests\userRequest;
use App\User;
use App\workgroupModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class userController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $u=Auth::user()->id;
        $item_name=DB::table('auth_assignment')->select('item_name')->where("user_id","=",$u)->first()->item_name;
        $menu_count=DB::table('auth_item_child')->where("item_name","=",$item_name)->where("child" ,"=", "view@1")->count();
        if($menu_count != 0) {
        $users=User::latest()->paginate(3);
        return view('admin.user.list_user',compact('users'));
        }else{
            echo '<img src="../../../images/pre/1.jpg" class="imgpre" style="margin-left:265px;margin-top:60px;border-radius: 80px;">';
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $u=Auth::user()->id;
        $item_name=DB::table('auth_assignment')->select('item_name')->where("user_id","=",$u)->first()->item_name;
        $menu_count=DB::table('auth_item_child')->where("item_name","=",$item_name)->where("child" ,"=", "create@1")->count();
        if($menu_count != 0) {
        return view('admin.user.add_user');
        }else{
            echo '<img src="../../../images/pre/1.jpg" class="imgpre" style="margin-left:265px;margin-top:60px;border-radius: 80px;">';
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(userRequest $request)
    {
        $record=new User();
        $record->name=$request->input('name');
        $record->email=$request->input('email');
        if($request->hasFile('picture')){
            $filename=time().'.'.$request->file('picture')->getClientOriginalExtension();
            if($request->file('picture')->move('images/pic',$filename)){
                $record->picture=$filename;
            }
        }
        $record->password=Hash::make($request->input('password'));
        $record->save();
        alert()->success('موفقیت امیز','با موفقیت بروزرسانی  شد');
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $uss=User::FindOrFail($request->exp_id);
        return view('admin.user.explain_user',compact('uss'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        $u=Auth::user()->id;
        $item_name=DB::table('auth_assignment')->select('item_name')->where("user_id","=",$u)->first()->item_name;
        $menu_count=DB::table('auth_item_child')->where("item_name","=",$item_name)->where("child" ,"=", "edit@1")->count();
        if($menu_count != 0) {
        return view('admin.user.edit_use',compact('user'));
        }else{
            echo '<img src="../../../images/pre/1.jpg" class="imgpre" style="margin-left:265px;margin-top:60px;border-radius: 80px;">';
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(userRequest $request,User $user)
    {
        $record =$request->all();
        if($request->hasFile('picture')){
            $filename=time().'.'.$request->file('picture')->getClientOriginalExtension();
            if($request->file('picture')->move('images/pic',$filename)){
                $record['picture'] = $filename;
            }
        }
        $user->update($record);
        $users=User::latest()->paginate(3);
        return view('admin.user.list_user',compact('users'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy(User $user)
    {
        $user->delete();
        alert()->success('موفقیت امیز','با موفقیت بروزرسانی  شد');
        return redirect()->back();
    }
    public function explain(){

    }
}
