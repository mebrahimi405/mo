<?php

namespace App\Http\Controllers\Administrator;

use App\Http\Controllers\Controller;
use App\workgroupModel;
use Illuminate\Http\Request;

class workController extends Controller
{
    public function active(Request $request,workgroupModel $workgroup)
    {
        $work=workgroupModel::FindOrFail($request->active_id);
        $work->update(['status'=>1]);
        alert()->success('فعال','با موفقیت فعال  شد')->showConfirmButton('تایید');
        return redirect()->back();

    }
    public function deactive(Request $request,workgroupModel $workgroup)
    {
        $word=workgroupModel::FindOrFail($request->deactive_id);
        $word->update(['status'=>0]);
        alert()->success('غیرفعال','با موفقیت غیرفعال  شد')->showConfirmButton('تایید');
        return redirect()->back();

    }
}
