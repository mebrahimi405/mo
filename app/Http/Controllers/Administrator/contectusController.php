<?php

namespace App\Http\Controllers\Administrator;

use App\contect;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class contectusController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $u=Auth::user()->id;
        $item_name=DB::table('auth_assignment')->select('item_name')->where("user_id","=",$u)->first()->item_name;
        $menu_count=DB::table('auth_item_child')->where("item_name","=",$item_name)->where("child" ,"=", "view@8")->count();
        if($menu_count != 0) {
            $connect = contect::all();
            return view( 'admin.contact.list_contact' , compact( 'connect' ) );
        }else{
            echo '<img src="../../../images/pre/1.jpg" class="imgpre" style="margin-left:265px;margin-top:60px;border-radius: 80px;">';

        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(contect $connect)
    {
        $u=Auth::user()->id;
        $item_name=DB::table('auth_assignment')->select('item_name')->where("user_id","=",$u)->first()->item_name;
        $menu_count=DB::table('auth_item_child')->where("item_name","=",$item_name)->where("child" ,"=", "edit@8")->count();
        if($menu_count != 0) {
        return view('admin.contact.add_contect',compact('connect'));
        }else{
            echo '<img src="../../../images/pre/1.jpg" class="imgpre" style="margin-left:265px;margin-top:60px;border-radius: 80px;">';

        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, contect $connect)
    {
        $id =$connect->ID;
        $name=$request->input('name');
        $family=$request->input('family');
        $email=$request->input('email');
        $phone=$request->input('phone');
        $content=$request->input('content');
        contect::where('id','=',$id)->update(array('name' => $name));
        contect::where('id','=',$id)->update(array('family' => $family));
        contect::where('id','=',$id)->update(array('email' => $email));
        contect::where('id','=',$id)->update(array('phone' => $phone));
        contect::where('id','=',$id)->update(array('content' => $content));
        alert()->success('موفقیت امیز','با موفقیت بروزرسانی شد');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy(contect $connect)
    {
$id=$connect->ID;

        $affectedRows = contect::where('ID', '=', $id)->delete();

        alert()->success('موفقیت امیز','با موفقیت بروزرسانی شد');
        return redirect()->back();

    }
}
