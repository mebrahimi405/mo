<?php

namespace App\Http\Controllers\Administrator;

use App\Http\Controllers\Controller;
use App\Http\Requests\slider;
use App\slideModel;
use Illuminate\Http\Request;

class sliderController extends Controller
{
    public function show(){
        $gallery=slideModel::all();
        return view('admin.slider.slider',compact('gallery'));
    }
    public function save(Request $request){
        $record =new slideModel();
        if($request->hasFile('img')){
            $filename=time().'.'.$request->file('img')->getClientOriginalExtension();
            if($request->file('img')->move('images/slide',$filename)){
                $record->img=$filename;
            }
        }
        $record->save();
        alert()->success('موفقیت امیز','با موفقیت بروزرسانی  شد');
        return redirect()->back();
    }
}
