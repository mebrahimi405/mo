<?php
namespace App\Http\Controllers\Administrator;
use App\categoryModel;
use App\Http\Controllers\Controller;
use App\newModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use PDF;

class pdfController extends Controller
{
    public function getpdf()
    {
        $new=DB::table('news')->join('category','category.id','=','news.category')->get();
        $data = [
            'foo' => 'bar'
        ];
        $pdf = PDF::loadView('admin.category.pdfcategory', $data,compact('new'));
        return $pdf->stream('document.pdf');

    }
}