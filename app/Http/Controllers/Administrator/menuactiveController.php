<?php

namespace App\Http\Controllers\Administrator;

use App\Http\Controllers\Controller;
use App\menuModel;
use Illuminate\Http\Request;

class menuactiveController extends Controller
{
    public function enable(Request $request,menuModel $menu)
    {
        $menu=menuModel::FindOrFail($request->enable_id);
        $menu->update(['status'=>1]);
        alert()->success('فعال','با موفقیت فعال  شد')->showConfirmButton('تایید');
        return redirect()->back();
    }
    public function disable(Request $request,menuModel $menu)
    {
        $menu=menuModel::FindOrFail($request->disable_id);
        $menu->update(['status'=>0]);
        alert()->success('غیرفعال','با موفقیت غیرفعال  شد')->showConfirmButton('تایید');
        return redirect()->back();
    }
}
