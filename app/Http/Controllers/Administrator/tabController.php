<?php

namespace App\Http\Controllers\Administrator;

use App\Http\Controllers\Controller;
use App\Http\Requests\tablrequest;
use App\newModel;
use App\tabModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class tabController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $u=Auth::user()->id;
        $item_name=DB::table('auth_assignment')->select('item_name')->where("user_id","=",$u)->first()->item_name;
        $menu_count=DB::table('auth_item_child')->where("item_name","=",$item_name)->where("child" ,"=", "view@2")->count();
        if($menu_count != 0) {
        $tab=tabModel::latest()->paginate(5);
        return view('admin.tab.list_tab',compact('tab'));
        }else{
            echo '<img src="../../../images/pre/1.jpg" class="imgpre" style="margin-left:265px;margin-top:60px;border-radius: 80px;">';
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $u=Auth::user()->id;
        $item_name=DB::table('auth_assignment')->select('item_name')->where("user_id","=",$u)->first()->item_name;
        $menu_count=DB::table('auth_item_child')->where("item_name","=",$item_name)->where("child" ,"=", "create@2")->count();
        if($menu_count != 0) {
        return view('admin.tab.add_tab');
        }else{
            echo '<img src="../../../images/pre/1.jpg" class="imgpre" style="margin-left:265px;margin-top:60px;border-radius: 80px;">';
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $record = new tabModel();
        if($request->hasFile('imgpic')){
            $filename = Time().'.'.$request->file('imgpic')->getClientOriginalExtension();
            if ($request->file('imgpic')->move('images/tablighat', $filename)) {
                $record->imgpic = $filename;
            }
        }
        $record->text=$request->input('text');
        $record->save();
        alert()->success('موفقیت امیز','با موفقیت درج  شد');
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $delt=tabModel::FindOrFail($request->explain_tab);
        return view('admin.tab.explain_tab',compact('delt'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(tabModel $tab)
    {
        $u=Auth::user()->id;
        $item_name=DB::table('auth_assignment')->select('item_name')->where("user_id","=",$u)->first()->item_name;
        $menu_count=DB::table('auth_item_child')->where("item_name","=",$item_name)->where("child" ,"=", "create@2")->count();
        if($menu_count != 0) {
            return view( 'admin.tab.edit_tab' , compact( 'tab' ) );
        }else{
            echo '<img src="../../../images/pre/1.jpg" class="imgpre" style="margin-left:265px;margin-top:60px;border-radius: 80px;">';
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, tabModel $tab)
    {
        $record=$request->all();
        if($request->hasFile('imgpic')){
            $filename = Time().'.'.$request->file('imgpic')->getClientOriginalExtension();
            if ($request->file('imgpic')->move('images/tablighat', $filename)) {
                $record['imgpic'] = $filename;
            }
        }
        $tab->update($record);
        alert()->success('موفقیت امیز','با موفقیت بروزرسانی شد  شد');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy(tabModel $tab)
    {
        $tab->delete();
        alert()->success('موفقیت امیز','با موفقیت حذف  شد');
        return redirect()->back();

    }
}
