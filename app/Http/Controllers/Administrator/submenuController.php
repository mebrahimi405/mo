<?php

namespace App\Http\Controllers\Administrator;

use App\faModel;
use App\Http\Controllers\Controller;
use App\menuModel;
use App\submenumodel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class submenuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $u=Auth::user()->id;
        $item_name=DB::table('auth_assignment')->select('item_name')->where("user_id","=",$u)->first()->item_name;
        $menu_count=DB::table('auth_item_child')->where("item_name","=",$item_name)->where("child" ,"=", "view@7")->count();
        if($menu_count != 0) {
            $submenuu = menuModel::join('subsystem', 'subsystem.menu_id','=','menu.id')->paginate(5);
            return view( 'admin.submenu.submenu_list',compact('submenuu'));
        }else{
            echo '<img src="../../../images/pre/1.jpg" class="imgpre" style="margin-left:265px;margin-top:60px;border-radius: 80px;">';

        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $u=Auth::user()->id;
        $item_name=DB::table('auth_assignment')->select('item_name')->where("user_id","=",$u)->first()->item_name;
        $menu_count=DB::table('auth_item_child')->where("item_name","=",$item_name)->where("child" ,"=", "create@7")->count();
        if($menu_count != 0) {
            $submenu = menuModel::all()->pluck( 'titleme' );
            $fa = faModel::all();
            return view( 'admin.submenu.add_submenu' , compact( 'submenu' , 'fa' ) );
        }else{
            echo '<img src="../../../images/pre/1.jpg" class="imgpre" style="margin-left:265px;margin-top:60px;border-radius: 80px;">';

        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $record=new submenumodel();
        $record->title=$request->input('title');
        $record->title_en=$request->input('title_en');
        $record->menu_id=$request->input('menu_id');
        $record->route=$request->input('route');
        $record->icon_class=$request->input('icon_class');
        $record->save();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $sub=submenumodel::FindOrFail($request->submenu_id);
        return view('admin.submenu.explain_sub',compact('sub'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(submenumodel $submenu)
    {
        $u=Auth::user()->id;
        $item_name=DB::table('auth_assignment')->select('item_name')->where("user_id","=",$u)->first()->item_name;
        $menu_count=DB::table('auth_item_child')->where("item_name","=",$item_name)->where("child" ,"=", "create@7")->count();
        if($menu_count != 0) {
            $submnu = menuModel::all()->pluck( 'titleme' );
            $fa = faModel::all();
            return view( 'admin.submenu.edit_submenu' , compact( 'submenu' , 'fa' , 'submnu' ) );
        }else{
            echo '<img src="../../../images/pre/1.jpg" class="imgpre" style="margin-left:265px;margin-top:60px;border-radius: 80px;">';

        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, submenumodel $submenu)
    {
        $record =$request->all();
        $submenu->update($record);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy(submenumodel $submenu)
    {
        $submenu->delete();
        alert()->success('موفقیت امیز','با موفقیت حذف  شد');
        return redirect()->back();
    }
}
