<?php

namespace App\Http\Controllers\Administrator;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class WelcomeController extends Controller
{
    public function index()
    {
        include(app_path() . '\Classes\phpgrid\jqgrid_dist.php');

        // Database config file to be passed in phpgrid constructor
        $db_conf = array(
            "type" 		=> 'mysqli',
            "server" 	=> 'localhost',
            "user" 		=> 'root',
            "password" 	=> '',
            "database" 	=> 'griddemo'
        );

        $g = new \jqgrid($db_conf);

        $opt["caption"] = "Sample Grid";
        $g->set_options($opt);

        $g->table = "clients";

        $out = $g->render("list1");

        return view('admin.category.list_category',array('phpgrid_output'=>$out));
    }
}
