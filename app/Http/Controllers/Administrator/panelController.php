<?php

namespace App\Http\Controllers\Administrator;
use App\Http\Controllers\Controller;
use App\menuModel;
use App\submenumodel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class panelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


    public function index()
    {
        $chart1 = \Chart::title([
            'text' => 'Voting ballon d`or 2018',
        ])
            ->chart([
                'type'     => 'column', // pie , columnt ect
                'renderTo' => 'chart1', // render the chart into your div with id
            ])
            ->subtitle([
            ])
            ->colors([
                '#0c2959'
            ])
            ->xaxis([
                'categories' => [

                ],
                'labels'     => [
                    'rotation'  => 15,
                    'align'     => 'top',
                    'formatter' => 'startJs:function(){return this.value + " (Footbal Player)"}:endJs',
                    // use 'startJs:yourjavasscripthere:endJs'
                ],
            ])
            ->yaxis([
                'text' => 'This Y Axis',
            ])
            ->legend([
                'layout'        => 'vertikal',
                'align'         => 'right',
                'verticalAlign' => 'middle',
            ])
            ->series(
                [
                    [
                        'name'  => 'Voting',
                        'data'  => [],
                        // 'color' => '#0c2959',
                    ],
                ]
            )
            ->display();

        $user=DB::table('users')->get();
        $news=DB::table('news')->get();
        $report=DB::table('reportimage')->get();
        $reporcheck=DB::table('reportimage')->where('status',1)->get();
        $newcheck=DB::table('news')->where('status',1)->get();
        $commtent=DB::table('comments')->get();
        $u=Auth::user()->id;
        $item_name=DB::table('auth_assignment')->select('item_name')->where("user_id","=",$u)->get();
        return view('admin.panel.home',compact('user','news','report','commtent','item_name','reporcheck','newcheck','chart1'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
