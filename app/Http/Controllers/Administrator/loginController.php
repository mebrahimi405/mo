<?php

namespace App\Http\Controllers\Administrator;

use App\Http\Controllers\Controller;
use App\loginModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class loginController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $u=Auth::user()->id;
        $item_name=DB::table('auth_assignment')->select('item_name')->where("user_id","=",$u)->first()->item_name;
        $menu_count=DB::table('auth_item_child')->where("item_name","=",$item_name)->where("child" ,"=", "view@9")->count();
        if($menu_count != 0) {
            $login = loginModel::all();
            return view( 'admin.login.list_login' , compact( 'login' ) );
        }else{
            echo '<img src="../../../images/pre/1.jpg" class="imgpre" style="margin-left:265px;margin-top:60px;border-radius: 80px;">';
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $u=Auth::user()->id;
        $item_name=DB::table('auth_assignment')->select('item_name')->where("user_id","=",$u)->first()->item_name;
        $menu_count=DB::table('auth_item_child')->where("item_name","=",$item_name)->where("child" ,"=", "view@9")->count();
        if($menu_count != 0) {
            $table = DB::table( 'login_config' )->select( '*' )->get();
            if (count( $table ) == 0) {
                return view( 'admin.login.add_login' );
            } elseif (count( $table ) > 0) {
                $login = loginModel::all();
                return view( 'admin.login.list_login' , compact( 'login' ) );
            }
        }else{
            echo '<img src="../../../images/pre/1.jpg" class="imgpre" style="margin-left:265px;margin-top:60px;border-radius: 80px;">';

        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $record = new loginModel();
        $record->title=$request->input('title');
        if($request->hasFile('logo')){
            $filename=time().'.'.$request->file('logo')->getClientOriginalExtension();
            if($request->file('logo')->move('images/login',$filename)){
                $record->logo=$filename;
            }
        }
        $record->copyright=$request->input('copyright');
        $record->save();
        alert()->success('موفقیت امیز','با موفقیت درج  شد');
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $login=loginModel::FindORFail($request->explain_login);
        return view('admin.login.expalin_login',compact('login'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(loginModel $login)
    {
        return view('admin.login.edit_login',compact('login'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, loginModel $login)
    {
        $record=$request->all();
        if($request->hasFile('logo')){
            $filename=time().'.'.$request->file('logo')->getClientOriginalExtension();
            if($request->file('logo')->move('images/login',$filename)){
                $record['logo']=$filename;
            }
        }
        $login->update($record);
        alert()->success('موفقیت امیز','با موفقیت بروزرسانی  شد');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
