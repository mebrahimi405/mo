<?php

namespace App\Http\Controllers\Administrator;

use App\commentModel;
use App\Http\Controllers\Controller;
use App\Http\Requests\commentRequest;
use Illuminate\Http\Request;

class commentController extends Controller
{
    public function show(){
        $comment=commentModel::all();
        return view('admin.comment.comment',compact('comment'));
    }

    /**
     * @param commentRequest $request
     * @param commentModel $commentt
     */
    public function update(Request $request, commentModel $commentt)
    {
        $commentt=commentModel::findOrFail($request->comment_id);
            $commentt->update(['accept'=>1]);
        alert()->success('موفقیت امیز','با موفقیت بروزرسانی  شد');
        return redirect()->back();
    }

    public function remove(Request $request,commentModel $commentt)
    {
        $comment=commentModel::findOrFail($request->comment_id);
        $comment->delete();
        alert()->success('موفقیت امیز','با موفقیت بروزرسانی  شد');
        return redirect()->back();
    }
}
