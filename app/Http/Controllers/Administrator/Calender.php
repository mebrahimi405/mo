<?php

namespace App\Http\Controllers\Administrator;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

class Calender extends Controller
{
    public function Chartjs(){
        $Events = array
        (
            "0" => array
            (
                "title" => "Event One",
                "start" => "2018-10-31",
            ),
            "1" => array
            (
                "title" => "Event Two",
                "start" => "2018-11-01",
            )
        );
        return view('admin.panel.home',['Events' => $Events]);
    }
}
