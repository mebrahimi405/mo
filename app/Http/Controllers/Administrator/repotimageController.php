<?php

namespace App\Http\Controllers\Administrator;

use App\Http\Controllers\Controller;
use App\Http\Requests\reportimagerequest;
use App\reportimageModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class repotimageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $u=Auth::user()->id;
        $item_name=DB::table('auth_assignment')->select('item_name')->where("user_id","=",$u)->first()->item_name;
        $menu_count=DB::table('auth_item_child')->where("item_name","=",$item_name)->where("child" ,"=", "view@5")->count();
        if($menu_count != 0) {
            $report = reportimageModel::all();
            return view( 'admin.reportimage.report_list' , compact( 'report' ) );
        }else{
            echo '<img src="../../../images/pre/1.jpg" class="imgpre" style="margin-left:265px;margin-top:60px;border-radius: 80px;">';
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $u=Auth::user()->id;
        $item_name=DB::table('auth_assignment')->select('item_name')->where("user_id","=",$u)->first()->item_name;
        $menu_count=DB::table('auth_item_child')->where("item_name","=",$item_name)->where("child" ,"=", "create@5")->count();
        if($menu_count != 0) {
            return view( 'admin.reportimage.report_add' );
        }else{
            echo '<img src="../../../images/pre/1.jpg" class="imgpre" style="margin-left:265px;margin-top:60px;border-radius: 80px;">';
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(reportimagerequest $request)
    {
        $record = new reportimageModel();
        $record->name=$request->input('name');
        $record->date=$request->input('date');
        $record->time=$request->input('time');
        $record->user=$request->input('user');
        if($request->hasFile('picture')){
            $filename=time().'.'.$request->file('picture')->getClientOriginalExtension();
            if($request->file('picture')->move('images/reportimage',$filename)){
                $record->picture=$filename;
            }
        }
        $record->save();
        alert()->success('موفقیت امیز','با موفقیت درج  شد');
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $re=reportimageModel::FindOrFail($request->explain_report);
        return view('admin.reportimage.explain_report',compact('re'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(reportimageModel $report)
    {
        $u=Auth::user()->id;
        $item_name=DB::table('auth_assignment')->select('item_name')->where("user_id","=",$u)->first()->item_name;
        $menu_count=DB::table('auth_item_child')->where("item_name","=",$item_name)->where("child" ,"=", "edit@5")->count();
        if($menu_count != 0) {
        return view('admin.reportimage.report_edit',compact('report'));
        }else{
            echo '<img src="../../../images/pre/1.jpg" class="imgpre" style="margin-left:265px;margin-top:60px;border-radius: 80px;">';
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,reportimageModel $report)
    {
        $record = $request->all();
        if($request->hasFile('picture')){
            $filename=time().'.'.$request->file('picture')->getClientOriginalExtension();
            if($request->file('picture')->move('images/reportimage',$filename)){
                $record['picture']=$filename;
            }
        }
        $report->update($record);
        alert()->success('موفقیت امیز','با موفقیت بروزرسانی  شد');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy(reportimageModel $report)
    {
        $report->delete();
        alert()->success('موفقیت امیز','با موفقیت حذف  شد');
        return redirect()->back();
    }
}
