<?php

namespace App\Http\Controllers\Administrator;

use App\Http\Controllers\Controller;
use App\submenumodel;
use Illuminate\Http\Request;

class subactiveController extends Controller
{
    public function enable(Request $request,submenumodel $submenu){
        $sub=submenumodel::FindOrFail($request->enable_id);
        $sub->update(['status'=>1]);
        alert()->success('فعال','با موفقیت فعال  شد')->showConfirmButton('تایید');
        return redirect()->back();
    }
    public function disable(Request $request,submenumodel $submenu)
    {
        $sub=submenumodel::FindOrFail($request->disable_id);
        $sub->update(['status'=>0]);
        alert()->success('غیرفعال','با موفقیت غیرفعال  شد')->showConfirmButton('تایید');
        return redirect()->back();

    }
}
