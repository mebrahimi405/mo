<?php

namespace App\Http\Controllers\Administrator;

use App\authModel;
use App\Http\Controllers\Controller;
use App\menuModel;
use App\premesionModel;
use App\submenumodel;
use App\workgroupModel;
use App\workmenuModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class workgroupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


    public function index()
    {
        $u=Auth::user()->id;
        $item_name=DB::table('auth_assignment')->select('item_name')->where("user_id","=",$u)->first()->item_name;
        $menu_count=DB::table('auth_item_child')->where("item_name","=",$item_name)->where("child" ,"=", "view@1")->count();
        if($menu_count != 0) {
            $workgroup = workgroupModel::all();
            return view( 'admin.workgroup.list_work' , compact( 'workgroup' ) );
            alert()->success( 'موفقیت امیز' , 'با موفقیت  این گروه کاری اضافه  شد' )->showConfirmButton( 'تایید' );
            return redirect()->back();
        }else{
            echo '<img src="../../../images/pre/1.jpg" class="imgpre" style="margin-left:265px;margin-top:60px;border-radius: 80px;">';
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $u=Auth::user()->id;
        $item_name=DB::table('auth_assignment')->select('item_name')->where("user_id","=",$u)->first()->item_name;
        $menu_count=DB::table('auth_item_child')->where("item_name","=",$item_name)->where("child" ,"=", "create@1")->count();
        if($menu_count != 0) {
            $menu = menuModel::all()->pluck( 'titleme' );
            $me = DB::table( 'menu' )->select( 'id' )->where( "id" , "=" , $menu )->first()->id;
//        $submenu=DB::table('subsystem')->join('menu','menu.id','=','subsystem.menu_id')->get();
            return view( 'admin.workgroup.add_work' , compact( 'menu' , 'me' ) );
        }else{
            echo '<img src="../../../images/pre/1.jpg" class="imgpre" style="margin-left:265px;margin-top:60px;border-radius: 80px;">';
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $record2=DB::table('auth_item_date')->where('title' , '=' , $request->input('title') )->count();
        if($record2==0){
        $record=new workgroupModel();
        $record->item_name=$request->input('item_name');
        $record->title=$request->input('title');
        $record->save();
        }

        $re=new workmenuModel();
        $re->item_name=$request->input('item_name');
        $re->menu_id=$request->input('menu_id');
        $re->save();

        $z=new authModel();
        $z->child=$request->input('child');
        $z->item_name=$request->input('item_name');
        $z->save();

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $wor=workgroupModel::FindOrFail($request->explain_work);
        return view('admin.workgroup.explain_workgroup',compact('wor'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(workgroupModel $workgroup)
    {
        $u=Auth::user()->id;
        $item_name=DB::table('auth_assignment')->select('item_name')->where("user_id","=",$u)->first()->item_name;
        $menu_count=DB::table('auth_item_child')->where("item_name","=",$item_name)->where("child" ,"=", "edit@1")->count();
        if($menu_count != 0) {
            return view( 'admin.workgroup.edit_work' , compact( 'workgroup' ) );
        }else{
            echo '<img src="../../../images/pre/1.jpg" class="imgpre" style="margin-left:265px;margin-top:60px;border-radius: 80px;">';
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, workgroupModel $workgroup)
    {
        $record=$request->all();
        $workgroup->update($record);
        alert()->success('موفقیت امیز','با موفقیت  این گروه کاری ویرایش  شد')->showConfirmButton('تایید');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy(workgroupModel $workgroup)
    {
        $workgroup->delete();
        alert()->success('موفقیت امیز','با موفقیت  این گروه کاری حذف   شد')->showConfirmButton('تایید');
        return redirect()->back();
    }
}
