<?php

namespace App\Http\Controllers\Administrator;

use App\categoryModel;
use App\Http\Controllers\Controller;
use App\Http\Requests\newRequest;
use App\Http\Requests\newsRequest;
use App\newModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use RealRashid\SweetAlert\Facades\Alert;

class newController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
//        $new=DB::table('news')->join('category','category.id','=','news.category')->paginate(6);
        $new = categoryModel::join('news', 'category.id','=','news.category')->get();
        return view('admin.new.list_new',compact('new'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $category=categoryModel::all()->pluck('name','id');
        return view('admin.new.add_new',compact('category'));
    }

    /**
     * Store a newly created resource in storage.
     *     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response

     */
    public function store(newsRequest $request)
    {
        $record3=DB::table('news')->where('title' , '=' , $request->input('title') )->count();
        if($record3==0){
        $record = new newModel();
        $record->title=$request->input('title');
        $record->date=$request->input('date');
        $record->time=$request->input('time');
        $record->user=$request->input('user');
        $record->context=$request->input('context');
        if($request->hasFile('image')){
            $filename=time().'.'.$request->file('image')->getClientOriginalExtension();
            if($request->file('image')->move('images/news',$filename)){
                $record->image=$filename;
            }
        }
        $record->category=$request->input('category');
        $record->type=$request->input('type');
        $record->source=$request->input('source');
        $record->status='0';
        $record->save();
        alert()->success('موفقیت امیز','با موفقیت درج  شد');
        return redirect()->back();
        }else{
            alert()->warning('هشدار','این خبر قبلا یکبار ثبت شده است');
            return redirect()->back();

        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $news=newModel::FindOrFail($request->explain_new);
        return view('admin.new.explain_new',compact('news'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(newModel $new)
    {
        $category=categoryModel::all()->pluck('name','id');
        return view('admin.new.edit_new',compact('new','category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,newModel $new)
    {
        $record =$request->all();
        if($request->hasFile('image')){
            $filename=time().'.'.$request->file('image')->getClientOriginalExtension();
            if($request->file('image')->move('images/news',$filename)){
                $record['image'] = $filename;
            }
        }
        $new->update($record);
        alert()->success('موفقیت امیز','با موفقیت بروزرسانی  شد');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy(newModel $new)
    {
        $new->delete();
        alert()->success('موفقیت امیز','با موفقیت حذف شد');
        return redirect()->back();
    }

}
