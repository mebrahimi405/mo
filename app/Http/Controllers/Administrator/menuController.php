<?php

namespace App\Http\Controllers\Administrator;

use App\faModel;
use App\Http\Controllers\Controller;
use App\menuModel;
use Illuminate\Http\Request;

class menuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $menu=menuModel::latest()->paginate(7);
        return view('admin.menu.menu_list',compact('menu'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $fa=faModel::all();
        return view('admin.menu.menu_add',compact('fa'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $record = new menuModel();
        $record->titleme=$request->input('titleme');
        $record->title_en=$request->input('title_en');
        $record->routeme=$request->input('routeme');
        $record->icon_classme=$request->input('icon_classme');
        $record->save();
        alert()->success('موفقیت امیز','با موفقیت درج  شد');
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $meee=menuModel::FindORFail($request->expalin_menu);
        return view('admin.menu.explain_menu',compact('meee'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(menuModel $menu)
    {
        $fa=faModel::all();
        return view('admin.menu.edit_menu',compact('menu','fa'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, menuModel $menu)
    {
        $record=$request->all();
        $menu->update($record);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy(menuModel $menu)
    {
        $menu->delete();
        alert()->success('موفقیت امیز','با موفقیت بروزرسانی  شد');
        return redirect()->back();
    }
}
