<?php

namespace App\Http\Controllers\Administrator;

use App\Http\Controllers\Controller;
use App\premesionModel;
use App\workgroupModel;
use Illuminate\Http\Request;

class premesionController extends Controller
{
    public function index(){
    }

        public function store(Request $request)
        {
            $record= new premesionModel();
            $record->user_id=$request->input('user_id');
            $record->item_name=$request->input('item_name');
            $record->save();
            alert()->success('موفقیت امیز','با موفقیت این کاربر به این گروه کاری اضافه  شد')->showConfirmButton('تایید');
            return redirect()->back();

        }
}
