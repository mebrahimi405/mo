<?php

namespace App\Http\Controllers\Administrator;

use App\Http\Controllers\Controller;
use App\reportimageModel;
use Illuminate\Http\Request;

class reController extends Controller
{
    public function update(Request $request,reportimageModel $report){
        $image=reportimageModel::FindOrFail($request->report_id);
        $image->update(['status'=>1]);
    }
}
