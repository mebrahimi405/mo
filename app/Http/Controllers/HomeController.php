<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        include(app_path() . '\Classes\phpgrid\jqgrid_dist.php');

        // Database config file to be passed in phpgrid constructor
        $db_conf = array(
            "type" 		=> 'mysqli',
            "server" 	=> 'localhost',
            "user" 		=> 'root',
            "password" 	=> '',
            "database" 	=> 'lara'
        );

        $g = new \jqgrid($db_conf);

        $opt["caption"] = "Sample Grid";
        $g->set_options($opt);

        $g->table = "category";

        $out = $g->render("list1");

        return view('welcome',array('phpgrid_output'=>$out));
    }

}
