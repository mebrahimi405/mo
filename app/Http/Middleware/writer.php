<?php

namespace App\Http\Middleware;

use Closure;

class writer
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(auth()->check()){
            if (auth()->user()->writer()){
                return $next($request);

            }
        }
        }
}
