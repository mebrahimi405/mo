<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class slideModel extends Model
{
    protected $table='slider';
    protected $fillable=['id','img'];

}
