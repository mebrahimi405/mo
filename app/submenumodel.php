<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class submenumodel extends Model
{
    protected $table="subsystem";
    protected $fillable=['id','title','title_en','menu_id','route','status'];

    public function menu(){
        $this->belongsTo(menuModel::class);
    }
}
