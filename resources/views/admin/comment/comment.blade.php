@extends('layout.index')
@section('content')
    <div class="row">
        <div class="col-sm-4" id="focomment">
            <div class="card">
                <div class="card-header">
                    <strong>تایید نظر</strong>
                </div>
                <div class="card-body">
                    @foreach($comment as $commentt)
                    <form action="\admin\agree" method="post"  class="form-horizontal">
                        {{@csrf_field()}}
                        {{@method_field('patch')}}
                        <div class="form-group row">
                            <div class="col-md-12">
                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-user"></i>
                                                    </span>
                                    <input type="text" id="input1-group1" name="name" value="{{$commentt->name}}"  class="form-control" placeholder="نام ">
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-12">
                                <div class="input-group">
                                    <input type="text" id="input2-group1" name="context" class="form-control" placeholder="محتوا" value="{{$commentt->context}}">
                                    <span class="input-group-addon"><i class="fa fa-text-height"></i>
                                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-12">
                                <div class="input-group">
                                    <input type="text" id="input2-group1" value="{{$commentt->user_id}}" name="user_id" class="form-control" placeholder="ایدی کاربر">
                                    <span class="input-group-addon"><i class="fa fa-user-times"></i>
                                                    </span>
                                </div>
                            </div>
                        </div>
                        <button type="submit"    class="btn btn-sm btn-success"><i class="fa fa-send"></i> ارسال</button>
                        <input type="hidden" name="comment_id" value="{{$commentt->id}}">

                    </form>

                </div>
                <div class="card-footer">
                    <form action="\admin\remove" method="post">
                        {{@method_field('delete')}}
                        {{@csrf_field()}}
                        <input type="hidden" name="comment_id" value="{{$commentt->id}}">
                        <button type="submit"  class="btn btn-sm btn-danger"><i class="fa fa-close"></i> حذف</button>

                    </form>
                    @endforeach

                </div>
            </div>
        </div>
    </div>

@endsection