 @extends('layout.index')
@section('script')
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="<?php echo url('../js/sort-table.js')?>"></script>
    <script src="<?php echo url('../js/sort-table.min.js')?>"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <script>
        $(document).ready(function(){
            $("#myInput").on("keyup", function() {
                var value = $(this).val().toLowerCase();
                $("#myTable tr").filter(function() {
                    $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
                });
            });
        });
    </script>


@endsection
@section('content')
    <div class="card">

        <div class="card-header">
            <h3 class="card-title" style="font-family: 'B Yekan';text-align: right;"><i class="fa fa-list-alt" style="position: relative;top: 2px;left: 4px;"></i>تنظیمات سایت</h3>




        </div>
        <!-- /.card-header -->

        <div class="card-body">
            <div class="row">

                <div class="col-lg-6" style="float: left!important;">
                    <input class="form-control" id="myInput" type="text" placeholder="جستجو...." style="margin-bottom: 20px;font-family: 'B Yekan'">
                </div>
            </div>
            <?php $tbl=DB::table('login_config')->get(); ?>
            @if(count($tbl)>=1)
                <table  class="table table-bordered table-striped js-sort-table"  id="demo1">
                    <thead>
                    <tr>
                        <th>عنوان وب سایت</th>
                        <th>تصویر وبسایت</th>
                        <th>عنوان کپی رایت</th>
                        <th>عملیات</th>
                    </tr>
                    </thead>

                    <tbody id="myTable" class="sortable">
                    @foreach($login as $log)
                        <tr style="font-family: 'B Yekan';text-align: center;">
                            <td>{{$log->title}}</td>
                            <td><img src="../../../images/login/{{$log->logo}}" width="50" height="50" alt=""></td>
                            <td>{{$log->copyright}}</td>
                            <td style="width: 12%">
                                <form action="{{route('login.edit',$log->id)}}"  style="width: 2%;">
                                    <button type="submit" class="btn btn-success" style="border-radius: 5px">
                                        <i class="fa fa-edit"></i>
                                    </button>
                                </form>
                                <form action="/admin/login/show"  style="width: 2%;">
                                    <input type="hidden" name="explain_login" value="{{$log->id}}">
                                    <button type="submit" class="btn btn-primary" style="border-radius: 5px;position: relative;top: -35px;right: 45px;">
                                        <i class="fa fa-eye"></i>
                                    </button>
                                </form>
                            </td>
                        </tr>
                    @endforeach

                    </tbody>
                    <tfoot>
                    <tr>
                        <th>عنوان وب سایت</th>
                        <th>تصویر وبسایت</th>
                        <th>عنوان کپی رایت</th>
                        <th>عملیات</th>
                    </tr>
                    </tfoot>
                </table>
            @else
                <p style="font-family: 'B Yekan'">چیزی برای نمایش وجود ندارد</p>
            @endif
        </div>
        <ul class="pagination">
        </ul>
        <!-- /.card-body -->
    </div>

@stop
