@extends('layout.index')
@section('content')

    <div class="row" style="width: 100%;height: 100%;position: relative;right:16px;font-family: 'B Yekan';font-size:20px;color: #000;">
        <div class="col-lg-12 bg-white">
            <p style="text-align: right;padding-right: 8px;padding-top:8px;box-sizing: border-box">جزئیات کاربر:{{$uss->name}}</p>
            <hr>
        <div class="col-lg-6 float-right" style="padding-top: 20px;box-sizing: border-box">
            <label for="">نام کاربر:{{$uss->name}}</label>
        </div>
        <div class="col-lg-6 float-left" style="padding-top: 20px;box-sizing: border-box">
            <label for="">ایمیل کاربر:{{$uss->email}}</label>
        </div>
<?php
            $pic=$uss->picture
            ?>
    @if($pic==true)
        <div class="col-lg-6 float-right" style="padding-top: 20px;box-sizing: border-box">
            <label for="">تصویرکاربر:<img src="../../images/pic/{{$pic}}" alt="" width="50" height="50">   </label>
        </div>
    @endif
        <div class="col-lg-6 float-left" style="padding-top: 20px;box-sizing: border-box">
            <form action="\admin\user\" style="position:relative;right:500px;top: 50px;">
            <button class="btn btn-primary" style="border-radius: 5px">بازگشت</button>
            </form>
        </div>
    </div>
    </div>
@endsection