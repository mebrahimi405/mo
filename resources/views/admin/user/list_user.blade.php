@extends('layout.index')
@section('script')
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="<?php echo url('../js/sort-table.js')?>"></script>
    <script src="<?php echo url('../js/sort-table.min.js')?>"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <script>
        $(document).ready(function(){
            $("#myInput").on("keyup", function() {
                var value = $(this).val().toLowerCase();
                $("#myTable tr").filter(function() {
                    $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
                });
            });
        });
    </script>


@endsection
@section('content')
    <div class="card">

        <div class="card-header">
            <h3 class="card-title" style="font-family: 'B Yekan';text-align: right;"><i class="fa fa-list-alt" style="position: relative;top: 2px;left: 4px;"></i>لیست کاربران</h3>

            <a href="\admin\user\create"><button type="button" class="bg bg-success" style="border-radius: 5px;"> <i class="fa fa-plus" aria-hidden="true"></i></button></a>



        </div>
        <!-- /.card-header -->

        <div class="card-body">
            <div class="row">

                <div class="col-lg-6" style="float: left!important;">
                    <input class="form-control" id="myInput" type="text" placeholder="جستجو...." style="margin-bottom: 20px;font-family: 'B Yekan'">
                </div>
            </div>
            <?php $tbl=DB::table('users')->get(); ?>
            @if(count($tbl)>=1)
                <table  class="table table-bordered table-striped js-sort-table"  id="demo1">
                    <thead>
                    <tr>
                        <th>نام کاربری</th>
                        <th>ایمیل</th>
                        <th>تصویر </th>
                        <th>سطح دسترسی</th>
                        <th>عملیات</th>
                    </tr>
                    </thead>

                    <tbody id="myTable" class="sortable">
                    @foreach($users as $use)
                        <tr>
                            <td>{{$use->name}}</td>
                            <td>{{$use->email}}</td>
                            <td><img src="../../images/pic/{{$use->picture}}" alt="" width="50" height="50"></td>
                            <td>{{$use->role}}</td>
                            <td style="width: 12%">
                                <form action="{{route('user.edit',$use->id)}}" style="width: 2%;">
                                    <button type="submit" class="btn btn-success" style="border-radius: 5px">
                                        <i class="fa fa-edit"></i>
                                    </button>
                                </form>
                                <form action="{{route('user.destroy',$use->id)}}" id="closee" method="post" style="width: 2%;">
                                    {{@method_field('delete')}}
                                    {{@csrf_field()}}
                                    <button type="submit" class="btn btn-danger" style="border-radius: 5px;position: relative;top: 13px;">
                                        <i class="fa fa-trash-o"></i>
                                    </button>
                                </form>
                                @if(! empty($_GET['item_name']))
                                    <form action="\admin\premesion\" id="closee" method="post" style="width: 2%;">
                                        {{@method_field('post')}}
                                        {{@csrf_field()}}
                                        <input type="hidden" name="user_id" value="{{$use->id}}">
                                        <input type="hidden" name="item_name" value="{{$_GET['item_name']}}">
                                        <button type="submit" class="btn btn-danger" style="border-radius: 5px">
                                            <i class="fa fa-plus"></i>
                                        </button>
                                    </form>
                                @endif
                                <form action="\admin\user\show" id="closee" method="get" style="width: 2%;">
                                    {{@method_field('get')}}
                                    {{@csrf_field()}}
                                    <input type="hidden" name="exp_id" value="{{$use->id}}">
                                    <button type="submit" class="btn btn-primary" style="border-radius: 5px;position: relative;top: -22px;right: 48px;">
                                        <i class="fa fa-plus"></i>
                                    </button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                    <tfoot>
                    <tr>
                        <th>نام کاربری</th>
                        <th>ایمیل</th>
                        <th>تصویر </th>
                        <th>سطح دسترسی</th>
                        <th>عملیات</th>
                    </tr>
                    </tfoot>
                </table>
            @else
                <p style="font-family: 'B Yekan'">چیزی برای نمایش وجود ندارد</p>
            @endif
        </div>
        <ul class="pagination">
            {{$users->links()}}
        </ul>
        <!-- /.card-body -->
    </div>

@stop

