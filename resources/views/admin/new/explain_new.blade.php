@extends('layout.index')
@section('content')

    <div class="row" style="width: 100%;height: 100%;position: relative;right:16px;font-family: 'B Yekan';font-size:20px;color: #000;">
        <div class="col-lg-12 bg-white">
            <p style="text-align: right;padding-right: 8px;padding-top:8px;box-sizing: border-box">عنوان خبر:{{$news->title}}</p>
            <hr>
            <div class="col-lg-6 float-left" style="padding-top: 20px;box-sizing: border-box">
                <label for="">عنوان خبر:{{$news->title}}</label>
            </div>
            <div class="col-lg-6 float-right" style="padding-top: 20px;box-sizing: border-box">
                <label for="">دسته بندی  خبر:{{$news->category}}</label>
            </div>
            <div class="col-lg-6 float-left" style="padding-top: 20px;box-sizing: border-box">
                <label for="">منبع  خبر:{{$news->source}}</label>
            </div>
            <div class="col-lg-6 float-right" style="padding-top: 20px;box-sizing: border-box">
                <label for="">نوع  خبر:{{$news->type}}</label>
            </div>
            <div class="col-lg-6 float-left" style="padding-top: 20px;box-sizing: border-box">
                <label for="">تصویر شاخص:<img src="../../images/news/{{$news->image}}" alt="" width="50" height="50">   </label>
            </div>
            <div class="col-lg-6 float-right" style="padding-top: 20px;box-sizing: border-box">
                <label for="">توضیحات خبر:</label>
            {{$news->context}}
            </div>
            <div class="col-lg-6 float-left" style="padding-top: 20px;box-sizing: border-box">
                <form action="\admin\new\" style="position:relative;right:982px;top:15px;">
                    <button class="btn btn-primary" style="border-radius: 5px">بازگشت</button>
                </form>
            </div>
        </div>
    </div>
@endsection