{{--@extends('layout.index')--}}
{{--@section('css')--}}
{{--@endsection--}}
{{--@section('script')--}}
    {{--<script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>--}}
    {{--<script>--}}
        {{--CKEDITOR.replace( 'context' );--}}
    {{--</script>--}}
{{--@endsection--}}
{{--@section('content')--}}
    {{--<div class="row">--}}
        {{--<div class="col-sm-6" id="form">--}}
            {{--{{Form::open(array('url'=>route('new.update',$new->id),'files'=>true,'class'=>'fouser'))}}--}}
    {{--{{@method_field('PATCH')}}--}}
            {{--<div class="card" >--}}
                {{--<form action="" >--}}

                    {{--<div class="card-header">--}}
                        {{--<strong>اضافه کردن خبرجدید</strong>--}}
                    {{--</div>--}}
                    {{--<div class="card-body">--}}
                        {{--<div class="form-group">--}}
                            {{--{{Form::label('inputname','عنوان خبر')}}--}}
                            {{--{{Form::text('title',$new->title,['class'=>'form-control'])}}--}}
                        {{--</div>--}}
                        {{--<div class="form-group">--}}
                            {{--{{Form::label('inputname','ادرس لینک')}}--}}
                            {{--{{Form::text('slug',$new->slug,['class'=>'form-control'])}}--}}
                        {{--</div>--}}
                        {{--<div class="form-group">--}}
                            {{--{{Form::label('inputname','توضیحات خبر')}}--}}
                            {{--{{Form::textarea('context',$new->context,['class'=>'form-control'])}}--}}
                        {{--</div>--}}
                        {{--<div class="form-group">--}}
                            {{--{{Form::label('inputname','تصویر خبر')}}--}}
                            {{--{{Form::file('image',['class'=>'form-control'])}}--}}
                            {{--<br>--}}
                            {{--<img src="../../../images/news/{{$new->image}}" alt="" width="80" height="80">--}}
                        {{--</div>--}}
                        {{--<div class="form-group">--}}
                            {{--{{Form::label('inputname','دسته بندی')}}--}}
                            {{--{{Form::select('category',$category,['class'=>'form-control'])}}--}}
                        {{--</div>--}}
                        {{--<div class="form-group">--}}
                            {{--{{Form::button('<i class="fa fa-send"></i> ارسال',['type'=>'submit','class'=>'btn btn-sm btn-primary'])}}--}}

                            {{--{{Form::button('<i class="fa fa-close"></i> انصراف',['type'=>'submit','class'=>'btn btn-sm btn-danger'])}}--}}
                        {{--</div>--}}

                        {{--<!--/.row-->--}}

                    {{--</div>--}}
                {{--</form>--}}
                {{--{{Form::close()}}--}}
            {{--</div>--}}

        {{--</div>--}}

    {{--</div>--}}
{{--@endsection--}}
@extends('layout.index')
@section('css')
    <style type="text/css">
        body {
            font-family: sans-serif;
            background-color: #eeeeee;
        }

        .file-upload {
            background-color: #ffffff;
            width: 600px;
            margin: 0 auto;
            padding: 20px;
        }

        .file-upload-btn {
            width: 100%;
            margin: 0;
            color: #fff;
            background: #1FB264;
            border: none;
            padding: 10px;
            border-radius: 4px;
            border-bottom: 4px solid #15824B;
            transition: all .2s ease;
            outline: none;
            text-transform: uppercase;
            font-weight: 700;
        }

        .file-upload-btn:hover {
            background: #1AA059;
            color: #ffffff;
            transition: all .2s ease;
            cursor: pointer;
        }

        .file-upload-btn:active {
            border: 0;
            transition: all .2s ease;
        }

        .file-upload-content {
            display: none;
            text-align: center;
        }

        .file-upload-input {
            position: absolute;
            margin: 0;
            padding: 0;
            width: 100%;
            height: 100%;
            outline: none;
            opacity: 0;
            cursor: pointer;
        }

        .image-upload-wrap {
            margin-top: 20px;
            border: 4px dashed #1FB264;
            position: relative;
        }

        .image-dropping,
        .image-upload-wrap:hover {
            background-color: #1FB264;
            border: 4px dashed #ffffff;
        }

        .image-title-wrap {
            padding: 0 15px 15px 15px;
            color: #222;
        }

        .drag-text {
            text-align: center;
        }

        .drag-text h3 {
            font-weight: 100;
            text-transform: uppercase;
            color:black;
            padding: 60px 0;
        }

        .file-upload-image {
            max-height: 200px;
            max-width: 200px;
            margin: auto;
            padding: 20px;
        }

        .remove-image {
            width: 200px;
            margin: 0;
            color: #fff;
            background: #cd4535;
            border: none;
            padding: 10px;
            border-radius: 4px;
            border-bottom: 4px solid #b02818;
            transition: all .2s ease;
            outline: none;
            text-transform: uppercase;
            font-weight: 700;
        }

        .remove-image:hover {
            background: #c13b2a;
            color: #ffffff;
            transition: all .2s ease;
            cursor: pointer;
        }

        .remove-image:active {
            border: 0;
            transition: all .2s ease;
        }
    </style>
@endsection
@section('script')
    <script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>
    <script>
        CKEDITOR.replace( 'context' );
    </script>
    <script type="text/javascript">
        function readURL(input) {
            if (input.files && input.files[0]) {

                var reader = new FileReader();

                reader.onload = function(e) {
                    $('.image-upload-wrap').hide();

                    $('.file-upload-image').attr('src', e.target.result);
                    $('.file-upload-content').show();

                    $('.image-title').html(input.files[0].name);
                };

                reader.readAsDataURL(input.files[0]);

            } else {
                removeUpload();
            }
        }

        function removeUpload() {
            $('.file-upload-input').replaceWith($('.file-upload-input').clone());
            $('.file-upload-content').hide();
            $('.image-upload-wrap').show();
        }
        $('.image-upload-wrap').bind('dragover', function () {
            $('.image-upload-wrap').addClass('image-dropping');
        });
        $('.image-upload-wrap').bind('dragleave', function () {
            $('.image-upload-wrap').removeClass('image-dropping');
        });
    </script>

@endsection
@section('content')

    <div class="row">
        <div class="col-sm-6" id="form">
            {{Form::open(array('url'=>route('new.update',$new->id),'files'=>true,'class'=>'fouser'))}}
            {{@method_field('PATCH')}}
            <div class="card" >
                <form action="" method="post">
                    @if(count($errors) >0)
                        <div class="alert alert-danger">
                            <ul>
                                @foreach($errors->all() as $error)
                                    <li>
                                        {{$error}}
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <div class="card-header">
                        <small></small>
                        <strong>اضافه کردن خبرجدید</strong>
                    </div>
                    <div class="card-body">
                        <div class="col-lg-6 float-right">
                            {{Form::label('inputname','عنوان خبر')}}
                            {{Form::text('title',$new->title,['class'=>'form-control'])}}
                        </div>
                        <div class="col-lg-6 float-left">
                            {{Form::label('inputname','دسته بندی')}}
                            {{Form::select('category',$category,['class'=>'form-control'])}}
                        </div>
                        <div class="col-lg-12 float-left">
                            {{Form::label('inputname','توضیحات خبر')}}
                            {{Form::textarea('context',$new->context,['class'=>'form-control'])}}
                        </div>
                        <div class="form-group">
                            {{Form::label('inputname','تصویر شاخص')}}<i class="fa fa-star" style="font-size: 10px;color: red"></i>

                            <div class="image-upload-wrap">

                                <input class="file-upload-input" type='file' name="image" onchange="readURL(this);" accept="image/*" />
                                <div class="drag-text">
                                    <h3><i class="fa fa-upload" aria-hidden="true" style="font-size: 50px;"></i>
                                    </h3>
                                </div>
                            </div>
                            <div class="file-upload-content">
                                <img class="file-upload-image" src="#" alt="your image" />
                                <div class="image-title-wrap">
                                    <button type="button" onclick="removeUpload()" class="remove-image">حذف <span class="image-title">Uploaded Image</span></button>

                                </div>

                            </div>
                            <img src="../../../images/news/{{$new->image}}" alt="" width="80" height="80">
                        </div>

                        <div class="col-lg-6 float-right">
                            {{Form::label('inputname','منبع خبر')}}
                            {{Form::text('source',$new->source,['class'=>'form-control'])}}
                        </div>
                        <div class="col-lg-6 float-left">
                            {{Form::label('inputname','نوع خیر')}}<i class="fa fa-star" style="font-size: 10px;color: red"></i>
                            {{Form::select('type',['خبر ویژه'=>'خبرویژه','خبر معمولی'=>'خبرمعمولی'],$new->type)}}
                        </div>
                        <div class="col-lg-6 float-right">
                        {{Form::label('inputname','ادرس لینک')}}
                        {{Form::text('slug',$new->slug,['class'=>'form-control'])}}
                        </div>
                        <div class="col-lg-6 float-left" style="position: relative;top: 35px;right: 470px;">
                            {{Form::button('<i class="fa fa-send"></i> ارسال',['type'=>'submit','class'=>'btn btn-sm btn-primary'])}}

                            {{Form::button('<i class="fa fa-close"></i> انصراف',['type'=>'reset','class'=>'btn btn-sm btn-danger'])}}
                        </div>

                        <!--/.row-->

                    </div>
                </form>
                {{Form::close()}}
            </div>

        </div>

    </div>
@endsection
