    @extends('layout.index')
@section('script')
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="<?php echo url('../js/sort-table.js')?>"></script>
    <script src="<?php echo url('../js/sort-table.min.js')?>"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <script>
        $(document).ready(function(){
            $("#myInput").on("keyup", function() {
                var value = $(this).val().toLowerCase();
                $("#myTable tr").filter(function() {
                    $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
                });
            });
        });
    </script>


@endsection
@section('content')
    <div class="card">

        <div class="card-header">
            <h3 class="card-title" style="font-family: 'B Yekan';text-align: right;"><i class="fa fa-list-alt" style="position: relative;top: 2px;left: 4px;"></i>لیست منو</h3>

            <a href="\admin\menu\create"><button type="button" class="bg bg-success" style="border-radius: 5px;"> <i class="fa fa-file-pdf-o" aria-hidden="true"></i></button></a>



        </div>
        <!-- /.card-header -->

        <div class="card-body">
            <div class="row">

                <div class="col-lg-6" style="float: left!important;">
                    <input class="form-control" id="myInput" type="text" placeholder="جستجو...." style="margin-bottom: 20px;font-family: 'B Yekan'">
                </div>
            </div>
            <?php $tbl=DB::table('menu')->get(); ?>
            @if(count($tbl)>=1)
                <table  class="table table-bordered table-striped js-sort-table"  id="demo1">
                    <thead>
                    <tr>
                        <th>کدمنو</th>
                        <th>عنوان منو</th>
                        <th>لینک</th>
                        <th>ایکون</th>
                        <th>وضعیت</th>
                        <th>عملیات</th>
                    </tr>
                    </thead>

                    <tbody id="myTable" class="sortable">
                    @foreach($menu as $me)
                        <tr style="font-family: 'B Yekan';text-align: center;">
                            <td>{{$me->id}}</td>
                            <td>{{$me->titleme}}</td>
                            <td>{{$me->routeme}}</td>
                            <td><i class="{{$me->icon_classme}}"></i></td>
                            <td>
                                @if($me->status==true)
                                    <span class="badge badge-pill badge-success">فعال</span>
                                @elseif($me->status==false)
                                    <span class="badge badge-pill badge-danger">غیرفعال</span>

                                @endif

                            </td>
                            <td style="width:17%">
                                <form action="{{route('menu.destroy',$me->id)}}" id="closee"  method="post" style="width: 2%;">
                                    {{@method_field('delete')}}
                                    {{@csrf_field()}}
                                    <button type="submit" class="btn btn-danger btnli"  style="border-radius: 5px">
                                        <i class="fa fa-trash-o"></i>
                                    </button>
                                </form>
                                <form  action="{{route('menu.edit',$me->id)}}"  style="width: 2%;position: relative;top: 7px;">
                                    <button type="submit" class="btn btn-success btnli" style="border-radius: 5px;">
                                        <i class="fa fa-edit"></i>
                                    </button>
                                </form>
                                @if($me->status==true)
                                    <form  action="/admin/disable/" method="post"  style="width: 2%;position: relative;top: 7px;">
                                        {{@csrf_field()}}
                                        {{@method_field('patch')}}
                                        <input type="hidden" name="disable_id" value="{{$me->id}}">
                                        <button type="submit" class="btn btn-primary btnli"  style="border-radius: 5px;">
                                            <i class="fa fa-check-circle-o"></i>
                                        </button>
                                    </form>

                                @elseif($me->status==false)
                                    <form  action="/admin/enable/" method="post"  style="width: 2%;position: relative;top: 7px;">
                                        {{@csrf_field()}}
                                        {{@method_field('patch')}}
                                        <input type="hidden" name="enable_id" value="{{$me->id}}">
                                        <button type="submit" class="btn btn-success btnli"  style="border-radius: 5px;">
                                            <i class="fa fa-check-circle-o"></i>
                                        </button>
                                    </form>

                                @endif
                                <form  action="/admin/menu/show" method="get"  style="width: 2%;position: relative;top: 7px;">
                                    {{@csrf_field()}}
                                    {{@method_field('get')}}
                                    <input type="hidden" name="expalin_menu" value="{{$me->id}}">
                                    <button type="submit" class="btn btn-success btnli"  style="border-radius: 5px;position: relative;top: -69px;right: 141px;">
                                        <i class="fa fa-eye"></i>
                                    </button>
                                </form>
                            </td>
                        </tr>
                    @endforeach

                    </tbody>
                    <tfoot>
                    <tr>
                        <th>کدمنو</th>
                        <th>عنوان منو</th>
                        <th>لینک</th>
                        <th>ایکون</th>
                        <th>وضعیت</th>
                        <th>عملیات</th>
                    </tr>
                    </tfoot>
                </table>
            @else
                <p style="font-family: 'B Yekan'">چیزی برای نمایش وجود ندارد</p>
            @endif
        </div>
        <ul class="pagination">
            {{$menu->links()}}
        </ul>
        <!-- /.card-body -->
    </div>

@stop

