{{--@extends('layout.index')--}}
{{--@section('content')--}}
    {{--<div class="col-sm-6" id="Formcategory">--}}
        {{--{{Form::open(array('url'=>route('workgroup.update',$workgroup->id)))}}--}}
        {{--{{@method_field('PATCH')}}--}}
        {{--<div class="card">--}}
            {{--<form action="" method="post" class="">--}}
                {{--<div class="card-header">--}}
                    {{--<strong>افزودن گروه کاری</strong>--}}
                {{--</div>--}}
                {{--<div class="card-body">--}}
                    {{--<div class="form-group">--}}
                        {{--{{Form::label('inputcategory','نام گروه کاری:')}}<i class="fa fa-star" style="font-size: 10px;color: red"></i>--}}
                        {{--<br>--}}
                        {{--{{Form::text('title',$workgroup->title,['placeholder'=>'نام گروه کاری ......','class'=>'form-control'])}}--}}
                    {{--</div>--}}

                    {{--<!--/.row-->--}}

                    {{--<div class="form-group">--}}
                        {{--{{Form::button('<i class="fa fa-send"></i>ارسال',['type'=>'submit','class'=>'btn btn-success'])}}--}}
                        {{--{{Form::button('<i class="fa fa-close"></i>انصراف',['type'=>'reset','class'=>'btn btn-danger'])}}--}}

                    {{--</div>--}}
                {{--</div>--}}
            {{--</form>--}}
        {{--</div>--}}

    {{--</div>--}}

{{--@endsection--}}
@extends('layout.index')
@section('script')
    <script type="text/javascript">
        $.fn.extend({
            treed: function (o) {

                var openedClass = 'glyphicon-minus-sign';
                var closedClass = 'glyphicon-plus-sign';

                if (typeof o != 'undefined'){
                    if (typeof o.openedClass != 'undefined'){
                        openedClass = o.openedClass;
                    }
                    if (typeof o.closedClass != 'undefined'){
                        closedClass = o.closedClass;
                    }
                };

                //initialize each of the top levels
                var tree = $(this);
                tree.addClass("tree");
                tree.find('li').has("ul").each(function () {
                    var branch = $(this); //li with children ul
                    branch.prepend("<i class='indicator glyphicon " + closedClass + "'></i>");
                    branch.addClass('branch');
                    branch.on('click', function (e) {
                        if (this == e.target) {
                            var icon = $(this).children('i:first');
                            icon.toggleClass(openedClass + " " + closedClass);
                            $(this).children().children().toggle();
                        }
                    })
                    branch.children().children().toggle();
                });
                //fire event from the dynamically added icon
                tree.find('.branch .indicator').each(function(){
                    $(this).on('click', function () {
                        $(this).closest('li').click();
                    });
                });
                //fire event to open branch if the li contains an anchor instead of text
                tree.find('.branch>a').each(function () {
                    $(this).on('click', function (e) {
                        $(this).closest('li').click();
                        e.preventDefault();
                    });
                });
                //fire event to open branch if the li contains a button instead of text
                tree.find('.branch>button').each(function () {
                    $(this).on('click', function (e) {
                        $(this).closest('li').click();
                        e.preventDefault();
                    });
                });
            }
        });

        //Initialization of treeviews

        $('#tree1').treed();

        $('#tree2').treed({openedClass:'glyphicon-folder-open', closedClass:'glyphicon-folder-close'});

        $('#tree3').treed({openedClass:'glyphicon-chevron-right', closedClass:'glyphicon-chevron-down'});

    </script>
    <script type="text/javascript">
        var toggler = document.getElementsByClassName("box");
        var i;

        for (i = 0; i < toggler.length; i++) {
            toggler[i].addEventListener("click", function() {
                this.parentElement.querySelector(".nested").classList.toggle("active");
                this.classList.toggle("check-box");
            });
        }
    </script>
@endsection
@section('content')
    <div class="row">
        <div class="col-sm-6" >
            {{Form::open(array('url'=>route('workgroup.update',$workgroup->id),'class'=>'fouser'))}}
            <div class="card">
                <form action="" method="post" class="">
                    <div class="card-header">
                        <strong>افزودن گروه کاری</strong>
                    </div>
                    <div class="card-body">
                        <div class="col-lg-6 float-right">
                            {{Form::label('inputcategory','نام گروه کاری:')}}<i class="fa fa-star" style="font-size: 10px;color: red"></i>
                            <br>
                            {{Form::text('title',$workgroup->title,['placeholder'=>'نام گروه کاری ......','class'=>'form-control'])}}
                        </div>

                    </div>

                    <div class="col-lg-6 float-left" style="position:relative;top: -55px;right:1150px;">
                        {{Form::button('<i class="fa fa-send"></i>ارسال',['type'=>'submit','class'=>'btn btn-success'])}}
                        {{Form::button('<i class="fa fa-close"></i>انصراف',['type'=>'reset','class'=>'btn btn-danger'])}}

                    </div>
            </div>
            </form>
        </div>
    </div>


@endsection