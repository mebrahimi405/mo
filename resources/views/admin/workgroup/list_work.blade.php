{{--@extends('layout.index');--}}
{{--@section('content')--}}
    {{--<div class="row">--}}
        {{--<div class="col-lg-12">--}}
            {{--<div class="card  table" id="fou">--}}
                {{--<div class="card-header">--}}
                    {{--<i class="fa fa-align-justify"></i> لیست کاربران--}}
                {{--</div>--}}
                {{--<div class="card-body">--}}
                    {{--<table class="table table-bordered table-striped table-sm table-dark table-hover"   >--}}
                        {{--<thead>--}}
                        {{--<tr>--}}
                            {{--<td>عنوان گروه کاری</td>--}}
                            {{--<td>وضعیت</td>--}}
                            {{--<td>عملیات</td>--}}
                        {{--</tr>--}}
                        {{--</thead>--}}
                        {{--<tbody>--}}
                        {{--@foreach($workgroup as $work)--}}
                            {{--<tr>--}}
                                {{--<td>{{$work->title}}</td>--}}
                                {{--<td>--}}
                                    {{--@if($work->status==true)--}}
                                        {{--<span class="badge badge-pill badge-success">فعا ل</span>--}}

                                    {{--@elseif($work->status==false)--}}
                                        {{--<span class="badge badge-pill badge-danger">غیرفعا ل</span>--}}

                                    {{--@endif--}}
                                {{--</td>--}}
                                {{--<td style="width: 20%">--}}
                                    {{--<form action="{{route('workgroup.edit',$work->id)}}"  style="width: 2%;">--}}
                                        {{--<button type="submit" class="btn btn-success" style="border-radius: 5px">--}}
                                            {{--<i class="fa fa-edit"></i>--}}
                                        {{--</button>--}}
                                    {{--</form>--}}
                                    {{--<form action="{{route('workgroup.destroy',$work->id)}}" id="closee" method="post" style="width: 2%;">--}}
                                        {{--{{@method_field('delete')}}--}}
                                        {{--{{@csrf_field()}}--}}
                                        {{--<button type="submit" class="btn btn-danger" style="border-radius: 5px">--}}
                                            {{--<i class="fa fa-trash-o"></i>--}}
                                        {{--</button>--}}
                                    {{--</form>--}}
                                  {{--@if($work->status==true)--}}
                                        {{--<form action="/admin/deactive/" id="closee" method="post" style="width: 2%;">--}}
                                            {{--{{@method_field('PATCH')}}--}}
                                            {{--{{@csrf_field()}}--}}
                                            {{--<input type="hidden" name="deactive_id" value="{{$work->id}}">--}}
                                            {{--<button type="submit" class="btn btn-danger" style="border-radius: 5px;position: relative;top: -48px;right: 51px;">--}}
                                                {{--<i class="fa fa-check-circle-o"></i>--}}
                                            {{--</button>--}}
                                        {{--</form>--}}
                                  {{--@elseif($work->status==false)--}}
                                        {{--<form action="/admin/active/" id="closee" method="post" style="width: 2%;">--}}
                                            {{--{{@method_field('PATCH')}}--}}
                                            {{--{{@csrf_field()}}--}}
                                            {{--<input type="hidden" name="active_id" value="{{$work->id}}">--}}
                                            {{--<button type="submit" class="btn btn-primary" style="border-radius: 5px;position: relative;top: -48px;right: 51px;">--}}
                                                {{--<i class="fa fa-check-circle-o"></i>--}}
                                            {{--</button>--}}
                                        {{--</form>--}}
                                  {{--@endif--}}
                                    {{--<form action="/admin/user/"  id="closee" style="width: 2%;">--}}
                                        {{--<input type="hidden" name="item_name" value="{{$work->item_name}}">--}}
                                        {{--<button type="submit" class="btn btn-success" style="border-radius: 5px;position: relative;top: -48px;right: 51px;">--}}
                                            {{--<i class="fa fa-user-plus"></i>--}}
                                        {{--</button>--}}
                                    {{--</form>--}}
                                {{--</td>--}}
                            {{--</tr>--}}
                        {{--@endforeach--}}
                        {{--</tbody>--}}
                    {{--</table>--}}
                    {{--<nav>--}}
                        {{--<ul class="pagination">--}}
{{--                            {{$category->links()}}--}}
                        {{--</ul>--}}
                    {{--</nav>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
        {{--<!--/.col-->--}}
    {{--</div>--}}

{{--@endsection--}}
@extends('layout.index')
@section('script')
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="<?php echo url('../js/sort-table.js')?>"></script>
    <script src="<?php echo url('../js/sort-table.min.js')?>"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <script>
        $(document).ready(function(){
            $("#myInput").on("keyup", function() {
                var value = $(this).val().toLowerCase();
                $("#myTable tr").filter(function() {
                    $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
                });
            });
        });
    </script>


@endsection
@section('content')
    <div class="card">

        <div class="card-header">
            <h3 class="card-title" style="font-family: 'B Yekan';text-align: right;"><i class="fa fa-list-alt" style="position: relative;top: 2px;left: 4px;"></i>لیست گروه کاری</h3>

            <a href="\admin\workgroup\create"><button type="button" class="bg bg-success" style="border-radius: 5px;"> <i class="fa fa-plus" aria-hidden="true"></i></button></a>



        </div>
        <!-- /.card-header -->

        <div class="card-body">
            <div class="row">

                <div class="col-lg-6" style="float: left!important;">
                    <input class="form-control" id="myInput" type="text" placeholder="جستجو...." style="margin-bottom: 20px;font-family: 'B Yekan'">
                </div>
            </div>
            <?php $tbl=DB::table('auth_item_date')->get(); ?>
            @if(count($tbl)>=1)
                <table  class="table table-bordered table-striped js-sort-table"  id="demo1">
                    <thead>
                    <tr>
                        <th>عنوان گروه کاری</th>
                        <th>وضعیت</th>
                        <th>عملیات</th>
                    </tr>
                    </thead>

                    <tbody id="myTable" class="sortable">
                    @foreach($workgroup as $work)
                    <tr>
                    <td>{{$work->title}}</td>
                    <td>
                    @if($work->status==true)
                    <span class="badge badge-pill badge-success">فعا ل</span>

                    @elseif($work->status==false)
                    <span class="badge badge-pill badge-danger">غیرفعا ل</span>

                    @endif
                    </td>
                    <td style="width: 20%">
                    <form action="{{route('workgroup.edit',$work->id)}}"  style="width: 2%;">
                    <button type="submit" class="btn btn-success" style="border-radius: 5px;position: relative;top: 37px;">
                    <i class="fa fa-edit"></i>
                    </button>
                    </form>
                    <form action="{{route('workgroup.destroy',$work->id)}}" id="closee" method="post" style="width: 2%;">
                    {{@method_field('delete')}}
                    {{@csrf_field()}}
                    <button type="submit" class="btn btn-danger" style="border-radius: 5px;position: relative;top: 50px;">
                    <i class="fa fa-trash-o"></i>
                    </button>
                    </form>
                    @if($work->status==true)
                    <form action="/admin/deactive/" id="closee" method="post" style="width: 2%;">
                    {{@method_field('PATCH')}}
                    {{@csrf_field()}}
                    <input type="hidden" name="deactive_id" value="{{$work->id}}">
                    <button type="submit" class="btn btn-danger" style="border-radius: 5px;position: relative;top:17px;right: 51px;">
                    <i class="fa fa-check-circle-o"></i>
                    </button>
                    </form>
                    @elseif($work->status==false)
                    <form action="/admin/active/" id="closee" method="post" style="width: 2%;">
                    {{@method_field('PATCH')}}
                    {{@csrf_field()}}
                    <input type="hidden" name="active_id" value="{{$work->id}}">
                    <button type="submit" class="btn btn-primary" style="border-radius: 5px;position: relative;top:15px;right: 51px;">
                    <i class="fa fa-check-circle-o"></i>
                    </button>
                    </form>
                    @endif
                    <form action="/admin/user/"  id="closee" style="width: 2%;">
                    <input type="hidden" name="item_name" value="{{$work->item_name}}">
                    <button type="submit" class="btn btn-success" style="border-radius: 5px;position: relative;top: -19px;right:98px;">
                    <i class="fa fa-user-plus"></i>
                    </button>
                    </form>
                    <form action="/admin/workgroup/show"  id="closee" style="width: 2%;">
                            <input type="hidden" name="explain_work" value="{{$work->id}}">
                            <button type="submit" class="btn btn-warning" style="border-radius: 5px;position: relative;top: -54px;right:144px;">
                                <i class="fa fa-eye"></i>
                            </button>
                    </form>
                    </td>
                    </tr>
                    @endforeach
                    </tbody>
                    <tfoot>
                    <tr>
                        <th>عنوان گروه کاری</th>
                        <th>وضعیت</th>
                        <th>عملیات</th>
                    </tr>
                    </tfoot>
                </table>
            @else
                <p style="font-family: 'B Yekan'">چیزی برای نمایش وجود ندارد</p>
            @endif
        </div>
        <ul class="pagination">
            {{--{{$users->links()}}--}}
        </ul>
        <!-- /.card-body -->
    </div>

@stop

