@extends('layout.index')
@section('css')
    <link href='../../j/fullcalendar.css' rel='stylesheet' />
    <link href='../../j/fullcalendar.print.css' rel='stylesheet' media='print' />
    <link href='../../dr/jquery-ui.structure.css' rel='stylesheet' media='print' />
    <link href='../../dr/jquery-ui.theme.css' rel='stylesheet' media='print' />
@endsection
@section('script')
    <script src='../../j/lib/moment.min.js'></script>
    <script src='../../j/lib/moment-jalaali.js'></script>
    <script src='../../j/lib/jquery.min.js'></script>
   <script src='../../j/lib/jquery/jquery-2.2.0.min.js'></script>
    <script src='../../j/fullcalendar.min.js'></script>
    <script src='../../j/locale-all.js'></script>
    <script src='../../dr/jquery-ui.js'></script>

    <script>
        function allowDrop(ev) {
            ev.preventDefault();
        }

        function drag(ev) {
            ev.dataTransfer.setData("text", ev.target.id);
        }

        function drop(ev) {
            ev.preventDefault();
            var data = ev.dataTransfer.getData("text");
            ev.target.appendChild(document.getElementById(data));
        }
    </script>

    <script>

        $(document).ready(function() {
            var initialLocaleCode = 'fa';

            $('#calendar').fullCalendar({
                header: {
                    left: 'prev,next today',
                    center: 'title',
                    right: 'month,agendaWeek,agendaDay,listMonth'
                },
                isJalaali : true,
                defaultDate: '2019-11-12',
                locale: initialLocaleCode,
                buttonIcons: false, // show the prev/next text
                weekNumbers: false,
                navLinks: true, // can click day/week names to navigate views
                editable: true,

            });

            // build the locale selector's options
            $.each($.fullCalendar.locales, function(localeCode) {
                $('#locale-selector').append(
                    $('<option/>')
                        .attr('value', localeCode)
                        .prop('selected', localeCode == initialLocaleCode)
                        .text(localeCode)
                );
            });

            // when the selected option changes, dynamically change the calendar option
            $('#locale-selector').on('change', function() {
                if (this.value) {
                    $('#calendar').fullCalendar('option', 'locale', this.value);
                    $('#calendar').fullCalendar('option', 'isJalaali', (this.value == 'fa' ? true : false));
                }
            });
        });

    </script>
    <script src="//code.highcharts.com/highcharts.js"></script>
    <script src="//code.highcharts.com/modules/series-label.js"></script>
    <script src="https://code.highcharts.com/modules/exporting.js"></script>
    <script src="https://code.highcharts.com/modules/export-data.js"></script>
    <script type="text/javascript">
        Highcharts.chart( {
                title: {
                    "text": "تعداد خبرها به تفکیک ماه"
                }
                , subtitle: {
                }
                , yAxis: {
                    "text": "ماه"
                }
                , xAxis: {
                    "categories":["فروردین", "اردیبهشت", "خرداد", "تیر" ,"مرداد","شهریور","مهر","ابان","اذر","دی","بهمن","اسفند"], "labels": {
                        "rotation":30, "align":"top", "formatter":function() {
                            return this.value
                        }
                    }
                }
                , legend: {
                    "layout": "vertikal", "align": "right", "verticalAlign": "middle"
                }
                <?php
          $DB=DB::table('news')->where('date' , 'LIKE','%/01/%')->count();
                $DB2=DB::table('news')->where('date' , 'LIKE','%/02/%')->count();
                $DB3=DB::table('news')->where('date' , 'LIKE','%/03/%')->count();
                $DB4=DB::table('news')->where('date' , 'LIKE','%/04/%')->count();
                $DB5=DB::table('news')->where('date' , 'LIKE','%/05/%')->count();
                $DB6=DB::table('news')->where('date' , 'LIKE','%/06/%')->count();
                $DB7=DB::table('news')->where('date' , 'LIKE','%/07/%')->count();
                $DB8=DB::table('news')->where('date' , 'LIKE','%/08/%')->count();
                $DB9=DB::table('news')->where('date' , 'LIKE','%/09/%')->count();
                $DB10=DB::table('news')->where('date' , 'LIKE','%/010/%')->count();
                $DB11=DB::table('news')->where('date' , 'LIKE','%/11/%')->count();
                $DB12=DB::table('news')->where('date' , 'LIKE','%/12/%')->count();
                //$array=array("$DB","$DB2","$DB3","$DB4","$DB5","$DB6","$DB7","$DB8","$DB9","$DB10","$DB11","$DB12")
            ?>
                , series: [ {
                    "name": "خبر", "data": [<?= $DB ?>,<?= $DB2;?>,<?= $DB3;?>,<?= $DB4;?>,<?= $DB5;?>,<?= $DB6;?>,<?= $DB7;?>,<?= $DB8;?>,<?= $DB9;?>,<?= $DB10;?>,<?= $DB11;?>,<?= $DB12;?>]
                }
                ], chart: {
                    "type": "column", "renderTo": "chart1"
                }
                , colors: ["#0c2959"], credits:false
            }

        );
    </script>
@endsection
@section('content')

    <!-- Breadcrumb -->
    <ol class="breadcrumb">
        <li class="breadcrumb-item">میزکار</li>

        <li class="breadcrumb-item active">میزکار</li>

        <!-- Breadcrumb Menu-->
     </ol>


    <div class="container-fluid">

        <div class="animated fadeIn">
            <div class="row" style="font-family:'B Yekan';font-size:17px;">
                <?php
                foreach ($item_name as $item):
                    $i=$item->item_name;
                endforeach;
                ?>
                @if($i=='afMxkYm' xor $i=='JxDIpfx' xor $i=='VDojJic')
                <div class="col-sm-6 col-lg-3">
                    <div class="card text-white bg-primary">
                        <div class="card-body pb-0">
                            <div class="btn-group float-right">
                            </div>

                            <h4 class="mb-0">{{$co=count($news)}}</h4>
                            <p>تعداد خبرها</p>
                            <i class="fa fa-newspaper-o" id="newspaper"></i>
                        </div>
                        <div class="chart-wrapper px-3" style="height:70px;">
                            <canvas id="card-chart1" class="chart" height="70"></canvas>
                        </div>
                    </div>
                </div>
                <!--/.col-->
                    @endif
                    <?php
                    foreach ($item_name as $item):
                        $i=$item->item_name;
                    endforeach;
                    ?>
                    @if($i=='afMxkYm' )
                <div class="col-sm-6 col-lg-3">
                    <div class="card text-white bg-info">
                        <div class="card-body pb-0">
                            <button type="button" class="btn btn-transparent p-0 float-right">
                            </button>
                            <h4 class="mb-0">{{$count=count($user)}}</h4>
                            <p>تعداد کاربران</p>
                            <i class="fa fa-users" id="users"></i>
                        </div>
                        <div class="chart-wrapper px-3" style="height:70px;">
                            <canvas id="card-chart2" class="chart" height="70"></canvas>
                        </div>
                    </div>
                </div>
                    @endif
                <!--/.col-->
                    <?php
                    foreach ($item_name as $item):
                        $i=$item->item_name;
                    endforeach;
                    ?>
                    @if($i=='afMxkYm' xor  $i=='WDuCsFi'  xor $i=='JxDIpfx')
                <div class="col-sm-6 col-lg-3">
                    <div class="card text-white bg-warning">
                        <div class="card-body pb-0">
                            <div class="btn-group float-right">
                            </div>
                            <h4 class="mb-0">{{$re=count($report)}}</h4>
                            <p>تعداد گزارش تصویری</p>
                            <i class="fa fa-image" id="picicon"></i>
                        </div>
                        <div class="chart-wrapper" style="height:70px;">
                            <canvas id="card-chart3" class="chart" height="70"></canvas>
                        </div>
                    </div>
                </div>
                    @endif
                <!--/.col-->
                    <?php
                    foreach ($item_name as $item):
                        $i=$item->item_name;
                    endforeach;
                    ?>
                    @if($i=='afMxkYm' xor $i=='JxDIpfx' )
                <div class="col-sm-6 col-lg-3">
                    <div class="card text-white bg-danger">
                        <div class="card-body pb-0">
                            <div class="btn-group float-right">
                            </div>
                            <h4 class="mb-0">{{$co=count($commtent)}}</h4>
                            <p>تعداد نظرات</p>
                            <i class="fa fa-comments-o" id="comment"></i>
                        </div>
                        <div class="chart-wrapper px-3" style="height:70px;">
                            <canvas id="card-chart4" class="chart" height="70"></canvas>
                        </div>
                    </div>
                </div>
                    @endif
                    <?php
                    foreach ($item_name as $item):
                        $i=$item->item_name;
                    endforeach;
                    ?>
                    @if($i=='afMxkYm' xor  $i=='WDuCsFi'  xor $i=='JxDIpfx' )
                    <div class="col-sm-6 col-lg-3">
                    <div class="card text-white bg-danger">
                        <div class="card-body pb-0">
                            <div class="btn-group float-right">
                            </div>
                            <h4 class="mb-0">{{$co=count($reporcheck)}}</h4>
                            <p>تایید گزارش تصویری</p>
                            <i class="fa fa-check" id="comment"></i>
                        </div>
                        <div class="chart-wrapper px-3" style="height:70px;">
                            <canvas id="card-chart4" class="chart" height="70"></canvas>
                        </div>
                    </div>
                </div>
                    @endif

                    <?php
                    foreach ($item_name as $item):
                        $i=$item->item_name;
                    endforeach;
                    ?>
                    @if($i=='afMxkYm' xor $i=='JxDIpfx' xor $i=='VDojJic')
                        <div class="col-sm-6 col-lg-3">
                            <div class="card text-white bg-danger">
                                <div class="card-body pb-0">
                                    <div class="btn-group float-right">
                                    </div>
                                    <h4 class="mb-0">{{$co=count($newcheck)}}</h4>
                                    <p>تایید خبر</p>
                                    <i class="fa fa-check" id="comment"></i>
                                </div>
                                <div class="chart-wrapper px-3" style="height:70px;">
                                    <canvas id="card-chart4" class="chart" height="70"></canvas>
                                </div>
                            </div>
                        </div>
                @endif
                <!--/.col-->

                <div style="direction: ltr">

                    <div id='calendar'  ></div>

                </div>
            </div>
            <div id="chart1"></div>

        {!! $chart1 !!}
            <!--/.row-->

            <!--/.card-->
            <!--/.row-->

            <!--/.row-->
        </div>


    </div>

@stop

