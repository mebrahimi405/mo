@extends('layout.index')
@section('content')
    <div class="row">
        <div class="col-sm-6">
            {{Form::open(array('url'=>route('category.update',$category->id),'method'=>'PATCH','class'=>'fouser'))}}

            <div class="card" >
                <form action="" method="post">
                    @if(count($errors) >0)
                        <div class="alert alert-danger">
                            <ul>
                                @foreach($errors->all() as $error)
                                    <li>
                                        {{$error}}
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <div class="card-header">
                        <strong>افزودن دسته بندی</strong>
                    </div>
                    <div class="card-body">
                        <div class="col-lg-6 float-right">
                            {{Form::label('inputcategory','نام دسته بندی')}}<i class="fa fa-star" style="font-size: 10px;color: red"></i>
                            <br>
                            {{Form::text('name',$category->name,['class'=>'form-control'])}}
                        </div>

                        <div class="col-lg-6 float-left" style="position: relative;top: 33px;right:500px;">
                            {{Form::button('<i class="fa fa-dot-circle-o"></i> ارسال',['type'=>'submit','class'=>'btn btn-sm btn-primary'])}}

                            {{Form::button('انصراف',['type'=>'reset','class'=>'btn btn-sm btn-danger'])}}
                        </div>

                        <!--/.row-->

                    </div>
                </form>
                {{Form::close()}}
            </div>

        </div>

    </div>
@endsection