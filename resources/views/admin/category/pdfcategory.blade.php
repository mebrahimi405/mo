<?php
use Hekmatinasser\Verta\Verta;

?>
<html >
<head>
    <title></title>

    <meta charset="UTF-8">
</head>
<body style="" >
<h1 class="titlepdf" style="text-align: center;">خبرها</h1>

<?php
date_default_timezone_set("Asia/Tehran");
$dt = new \Datetime('');
$ti= new Verta($dt); // 1395-12-09 15:05:56

?>
<p><?php echo $ti.':زمان و تاریخ پرینت '?></p>
<table border="1"  style="text-align: center;direction: rtl;width:100%;margin: auto;">
    <tr style="background: gray">
        <td >کد</td>
        <td>عنوان</td>
        <td>لینک</td>
        <td>تاریخ ثبت</td>
        <td>زمان ثبت</td>
        <td>کاربرثبت</td>
        <td>محتوا</td>
        <td>دسته بندی</td>
        <td>نوع خبر</td>
        <td>منبع</td>
        <td>وضعیت</td>
    </tr>
    @foreach($new as $news)
    <tr>
        <td >{{$news->id}}</td>
        <td>{{$news->title}}</td>
        <td>{{$news->slug}}</td>
        <td>{{$news->date}}</td>
        <td>{{$news->time}}</td>
        <td>{{$news->user}}</td>
        <td>{{$news->context}}</td>
        <td>{{$news->name}}</td>
        <td>{{$news->type}}</td>
        <td>{{$news->source}}</td>
        <td>
            <?php
            if($news->status==true){
                echo 'تایید';
            }
            elseif ($news->status==false){
                echo 'تایید نشده';

            }

            ?>
        </td>
    </tr>
    @endforeach
</table>
</body>
</html>