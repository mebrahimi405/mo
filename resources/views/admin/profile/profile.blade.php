@extends('layout.index')
@section('content')
    <div class="leftpro">

        <div class="card" id="profile" style="width:400px">
            <img class="card-img-top" src="../../../images/pic/{{Auth::user()->picture}}" alt="Card image" style="width:100%">
            <div class="card-body">
                <h4 class="card-title">{{Auth::user()->name}}</h4>
                <p class="card-text">{{Auth::user()->email}}</p>
            </div>
        </div>

    </div>
    <div class="rightpro">
        <div class="row">
            <div class="col-sm-6" id="form">
                {{Form::open(array('action'=>'Administrator\profileController@store','files'=>true,'class'=>'foprofile'))}}

                <div class="card" >
                    <form action="" method="post">
                        <div class="card-header">
                            <small></small>
                            <strong>پروفایل کاربری</strong>
                        </div>
                        <div class="card-body">
                            <div class="form-group">
                                {{Form::label('inputname','نام کاربر:')}}
                                {{Form::text('name',Auth::user()->name,['class'=>'form-control'])}}
                            </div>

                            <div class="form-group">
                                {{Form::label('inputname','تصویر کاربر')}}
                                {{Form::file('picture',['class'=>'form-control'])}}
                                <br>
                                <img src="../../../images/pic/{{Auth::user()->picture}}" alt="" width="50" height="50">
                            </div>
                            <div class="form-group">
                                {{Form::label('inputname','رمزعبور')}}
                                {{Form::password('password',['class'=>'form-control'])}}
                            </div>
                            <div class="form-group">
                                {{Form::label('inputname','ایمیل کاربر')}}
                                {{Form::text('email',Auth::user()->email,['class'=>'form-control'])}}
                            </div>
                            <div class="form-group">
                                <input type="hidden" value="{{Auth::user()->id}}" name="user_id">
                                {{Form::button('<i class="fa fa-send"></i> ویرایش اطلاعات',['type'=>'submit','class'=>'btn btn-sm btn-primary','id'=>'btnpr'])}}
                            </div>

                            <!--/.row-->

                        </div>
                    </form>
                    {{Form::close()}}
                </div>

            </div>

        </div>
    </div>
@endsection
