@extends('layout.index')
@section('content')
    <div class="row">
        <div class="col-sm-6" id="form">
            {{Form::open(array('url'=>route('connect.update',$connect->ID),'class'=>'fouser'))}}
            {{@method_field('PATCH')}}

            <div class="card" >
                <form action="" method="post">
                    <div class="card-header">
                        <small></small>
                        <strong>ویرایش تماس با ما</strong>
                    </div>
                    <div class="card-body">
                        <div class="form-group">
                            {{Form::label('inputname','نام')}}<i class="fa fa-star" style="font-size: 10px;color: red"></i>
                            {{Form::text('name',$connect->name,['class'=>'form-control'])}}
                        </div>
                        <div class="form-group">
                            {{Form::label('inputname','فامیل')}}<i class="fa fa-star" style="font-size: 10px;color: red"></i>
                            {{Form::text('family',$connect->family,['class'=>'form-control'])}}
                        </div>
                        <div class="form-group">
                            {{Form::label('inputname','ایمیل')}}<i class="fa fa-star" style="font-size: 10px;color: red"></i>
                            {{Form::text('email',$connect->email,['class'=>'form-control'])}}
                        </div>
                        <div class="form-group">
                            {{Form::label('inputname','تلفن')}}
                            {{Form::text('phone',$connect->phone,['class'=>'form-control'])}}
                        </div>
                        <div class="form-group">
                            {{Form::label('inputname','محتوا')}}
                            {{Form::text('content',$connect->content,['class'=>'form-control'])}}
                        </div>
                        <div class="form-group">
                            {{Form::button('<i class="fa fa-send"></i> ارسال',['type'=>'submit','class'=>'btn btn-sm btn-primary'])}}
                            {{Form::button('<i class="fa fa-close"></i> انصراف',['type'=>'reset','class'=>'btn btn-sm btn-danger'])}}
                        </div>
                        <!--/.row-->

                    </div>
                </form>
                {{Form::close()}}
            </div>

        </div>

    </div>
@endsection
