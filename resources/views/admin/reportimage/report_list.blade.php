

@extends('layout.index')
@section('script')
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="<?php echo url('../js/sort-table.js')?>"></script>
    <script src="<?php echo url('../js/sort-table.min.js')?>"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <script>
        $(document).ready(function(){
            $("#myInput").on("keyup", function() {
                var value = $(this).val().toLowerCase();
                $("#myTable tr").filter(function() {
                    $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
                });
            });
        });
    </script>


@endsection
@section('content')
    <div class="card">

        <div class="card-header">
            <h3 class="card-title" style="font-family: 'B Yekan';text-align: right;"><i class="fa fa-list-alt" style="position: relative;top: 2px;left: 4px;"></i>لیست گزارش تصویری</h3>

            <a href="\admin\getpdf"><button type="button" class="bg bg-success" style="border-radius: 5px;"> <i class="fa fa-file-pdf-o" aria-hidden="true"></i></button></a>
            <a href="\admin\report\create"><button type="button" class="bg bg-success" style="border-radius: 5px;"> <i class="fa fa-plus" aria-hidden="true"></i></button></a>



        </div>
        <!-- /.card-header -->

        <div class="card-body">
            <div class="row">

                <div class="col-lg-6" style="float: left!important;">
                    <input class="form-control" id="myInput" type="text" placeholder="جستجو...." style="margin-bottom: 20px;font-family: 'B Yekan'">
                </div>
            </div>
            <?php $tbl=DB::table('reportimage')->get(); ?>
            @if(count($tbl)>=1)
                <table  class="table table-bordered table-striped js-sort-table"  id="demo1">
                    <thead>
                    <tr>
                        <th>کد گزارش </th>
                        <th>نام گزارش </th>
                        <th>تاریخ ثبت </th>
                        <th>زمان ثبت </th>
                        <th>کاربر ثبت </th>
                        <th>تصویر گزارش </th>
                        <th>وضعیت</th>
                        <th>عملیات</th>
                    </tr>
                    </thead>

                    <tbody id="myTable" class="sortable">
                    @foreach($report as $image)
                        <tr>
                            <td>{{$image->id}}</td>
                            <td>{{$image->name}}</td>
                            <td>{{$image->date}}</td>
                            <td>{{$image->time}}</td>
                            <td>{{$image->user}}</td>
                            <td><img src="../../../images/reportimage/{{$image->picture}}" alt="" width="50" height="50"></td>
                            <td>
                                @if($image->status==true)
                                    <span class="badge badge-success">تایید شده</span>
                                @elseif($image->status==false)
                                    <span class="badge badge-danger">تایید  نشده</span>


                                @endif
                            </td>
                            <td style="width: 17%">
                                <form action="{{route('report.edit',$image->id)}}"  style="width: 2%;">
                                    <button type="submit" class="btn btn-success btna" style="border-radius: 5px">
                                        <i class="fa fa-edit"></i>
                                    </button>
                                </form>
                                <form action="{{route('report.destroy',$image->id)}}" id="closee" method="post" style="width: 2%;">
                                    {{@method_field('delete')}}
                                    {{@csrf_field()}}
                                    <button type="submit" class="btn btn-danger btnad" style="border-radius: 5px">
                                        <i class="fa fa-trash-o"></i>
                                    </button>
                                </form>
                                <?php
                                $u=Auth::user()->id;
                                $item_name=DB::table('auth_assignment')->select('item_name')->where("user_id","=",$u)->get();
                                ?>
                                <?php
                                foreach ($item_name as $item):
                                    $i=$item->item_name;
                                endforeach;
                                ?>
                                @if($i=='afMxkYm' xor $i=='JxDIpfx')
                                    <form action="/admin/reupdate/" id="refresh" method="post"   style="width: 2%;">
                                        {{@csrf_field()}}
                                        {{@method_field('patch')}}
                                        <input type="hidden" name="report_id" value="{{$image->id}}">
                                        <button type="submit" class="btn btn-success btnsd" style="border-radius: 5px">
                                            <i class="fa fa-check"></i>
                                        </button>
                                    </form>
                                @endif
                                <form action="/admin/report/show" id="refresh" method="get"   style="width: 2%;">
                                    {{@csrf_field()}}
                                    {{@method_field('get')}}
                                    <input type="hidden" name="explain_report" value="{{$image->id}}">
                                    <button type="submit" class="btn btn-primary btnsd" style="border-radius: 5px;position: relative;top:16px;right:44px;">
                                        <i class="fa fa-eye"></i>
                                    </button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                    <tfoot>
                    <tr>
                        <th>کد گزارش </th>
                        <th>نام گزارش </th>
                        <th>تاریخ ثبت </th>
                        <th>زمان ثبت </th>
                        <th>کاربر ثبت </th>
                        <th>تصویر گزارش </th>
                        <th>وضعیت</th>
                        <th>عملیات</th>
                    </tr>
                    </tfoot>
                </table>
            @else
                <p style="font-family: 'B Yekan'">چیزی برای نمایش وجود ندارد</p>
            @endif
        </div>
        <ul class="pagination">
        </ul>
        <!-- /.card-body -->
    </div>

@stop

