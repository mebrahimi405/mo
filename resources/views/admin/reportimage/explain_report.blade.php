@extends('layout.index')
@section('content')

    <div class="row" style="width: 100%;height: 100%;position: relative;right:16px;font-family: 'B Yekan';font-size:20px;color: #000;">
        <div class="col-lg-12 bg-white">
            <p style="text-align: right;padding-right: 8px;padding-top:8px;box-sizing: border-box">گزارش تصویری</p>
            <hr>
            <div class="col-lg-6 float-right" style="padding-top: 20px;box-sizing: border-box">
                <label for="">نام گزارش تصویری:{{$re->name}}</label>
            </div>
                <div class="col-lg-6 float-right" style="padding-top: 20px;box-sizing: border-box">
                    <label for="">تصویر گزارش تصویری:<img src="../../../images/reportimage/{{$re->picture}}" alt="" width="50" height="50">   </label>
                </div>
            <div class="col-lg-6 float-left" style="padding-top: 20px;box-sizing: border-box">
                <form action="\admin\report\" style="position:relative;right:500px;top: 50px;">
                    <button class="btn btn-primary" style="border-radius: 5px">بازگشت</button>
                </form>
            </div>
        </div>
    </div>
@endsection