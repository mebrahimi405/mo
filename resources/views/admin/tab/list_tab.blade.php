@extends('layout.index')
@section('script')
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="<?php echo url('../js/sort-table.js')?>"></script>
    <script src="<?php echo url('../js/sort-table.min.js')?>"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <script>
        $(document).ready(function(){
            $("#myInput").on("keyup", function() {
                var value = $(this).val().toLowerCase();
                $("#myTable tr").filter(function() {
                    $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
                });
            });
        });
    </script>


@endsection
@section('content')
    <div class="card">

        <div class="card-header">
            <h3 class="card-title" style="font-family: 'B Yekan';text-align: right;"><i class="fa fa-list-alt" style="position: relative;top: 2px;left: 4px;"></i>لیست تبلیغات</h3>

            <a href="\admin\tab\create"><button type="button" class="bg bg-success" style="border-radius: 5px;"> <i class="fa fa-plus" aria-hidden="true"></i></button></a>



        </div>
        <!-- /.card-header -->

        <div class="card-body">
            <div class="row">

                <div class="col-lg-6" style="float: left!important;">
                    <input class="form-control" id="myInput" type="text" placeholder="جستجو...." style="margin-bottom: 20px;font-family: 'B Yekan'">
                </div>
            </div>
            <?php $tbl=DB::table('advertising')->get(); ?>
            @if(count($tbl)>=1)
                <table  class="table table-bordered table-striped js-sort-table"  id="demo1">
                    <thead>
                    <tr>
                        <th>کد تبلیغات</th>
                        <th>عکس</th>
                        <th>متن</th>
                        <th>عملیات</th>
                    </tr>
                    </thead>

                    <tbody id="myTable" class="sortable">
                    @foreach($tab as $tablighat)
                    <tr>
                    <td>{{$tablighat->id}}</td>
                    <td><img src="../../../images/tablighat/{{$tablighat->imgpic}}" width="80" height="80" alt=""></td>
                    <td>{{$tablighat->text}}</td>
                    <td style="width: 12%">
                    <form action="{{route('tab.edit',$tablighat->id)}}"  style="width: 2%;">
                    <button type="submit" class="btn btn-success" style="border-radius: 5px">
                    <i class="fa fa-edit"></i>
                    </button>
                    </form>
                    <form action="{{route('tab.destroy',$tablighat->id)}}" id="closee" method="post" style="width: 2%;">
                    {{@method_field('delete')}}
                    {{@csrf_field()}}
                    <button type="submit" class="btn btn-danger" style="border-radius: 5px;position: relative;top: 13px;">
                    <i class="fa fa-trash-o"></i>
                    </button>
                    </form>
                        <form action="/admin/tab/show" id="closee" method="get" style="width: 2%;">
                            {{@method_field('get')}}
                            {{@csrf_field()}}
                            <input type="hidden" name="explain_tab" value="{{$tablighat->id}}">
                            <button type="submit" class="btn btn-primary" style="border-radius: 5px;position: relative;top:-22px;right: 46px;">
                                <i class="fa fa-eye"></i>
                            </button>
                        </form>
                    </td>
                    </tr>
                    @endforeach
                    </tbody>
                    <tfoot>
                    <tr>
                        <th>کد تبلیغات</th>
                        <th>عکس</th>
                        <th>متن</th>
                        <th>عملیات</th>
                    </tr>
                    </tfoot>
                </table>
            @else
                <p style="font-family: 'B Yekan'">چیزی برای نمایش وجود ندارد</p>
            @endif
        </div>
        <ul class="pagination">
            {{$tab->links()}}
        </ul>
        <!-- /.card-body -->
    </div>

@stop

