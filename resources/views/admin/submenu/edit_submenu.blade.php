@extends('layout.index')
@section('css')
    <link rel="stylesheet" href="<?php echo url('../css/bootstrap-iconpicker.min.css')?>">
    <link rel="stylesheet" type="text/css" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css">

@endsection
@section('script')
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <script type="text/javascript" src="<?php echo url('../js/bootstrap-iconpicker-iconset-all.js')?>"></script>
    <script type="text/javascript" src="<?php echo url('../js/bootstrap-iconpicker-iconset-all.min.js')?>"></script>
    <script type="text/javascript" src="<?php echo url('../js/bootstrap-iconpicker.bundle.min.js')?>"></script>
    <script type="text/javascript" src="<?php echo url('../js/bootstrap-iconpicker.js')?>"></script>
    <script type="text/javascript" src="<?php echo url('../js/bootstrap-iconpicker.min.js')?>"></script>
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>


@endsection
@section('content')

    <div class="row">
        <div class="col-sm-6" id="form">
            {{Form::open(array('url'=>route('submenu.update',$submenu->id),'files'=>true,'class'=>'fouser'))}}
            {{@method_field('PATCH')}}

            <div class="card" >
                <form action="" method="post">
                    @if(count($errors) >0)
                        <div class="alert alert-danger">
                            <ul>
                                @foreach($errors->all() as $error)
                                    <li>
                                        {{$error}}
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <div class="card-header">
                        <small></small>
                        <strong>اضافه کردن زیرمنو</strong>
                    </div>
                    <div class="card-body">
                        <div class="col-lg-6 float-right">
                            {{Form::label('inputname','عنوان زیرمنو')}}
                            {{Form::text('title',$submenu->title,['class'=>'form-control'])}}
                        </div>
                        <div class="col-lg-6 float-left">
                            {{Form::label('inputname','عنوان انگلیسی زیرمنو')}}
                            {{Form::text('title_en',$submenu->title_en,['class'=>'form-control'])}}
                        </div>
                        <div class="col-lg-6 float-right">
                            {{Form::label('inputname','لینک زیرمنو')}}
                            {{Form::text('route',$submenu->route,['class'=>'form-control'])}}
                        </div>
                        <div class="col-lg-6 float-left">
                            {{Form::label('inputname','ایکون زیرمنو')}}
                            <button class="btn btn-secondary form-control" name="icon_class" role="iconpicker" value=""></button>
                        </div>
                        <div class="col-lg-6 float-right">
                            {{Form::label('inputname','منو')}}
                            {{Form::select('menu_id',$submnu)}}
                        </div>

                        <div class="col-lg-6 float-left" style="position: relative;top:30px;right:470px;">
                            {{Form::button('<i class="fa fa-send"></i> ارسال',['type'=>'submit','class'=>'btn btn-sm btn-primary'])}}

                            {{Form::button('<i class="fa fa-close"></i> انصراف',['type'=>'reset','class'=>'btn btn-sm btn-danger'])}}
                        </div>

                        <!--/.row-->

                    </div>
                </form>
                {{Form::close()}}
            </div>

        </div>

    </div>
@endsection
