@extends('layout.index')
@section('script')
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="<?php echo url('../js/sort-table.js')?>"></script>
    <script src="<?php echo url('../js/sort-table.min.js')?>"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <script>
        $(document).ready(function(){
            $("#myInput").on("keyup", function() {
                var value = $(this).val().toLowerCase();
                $("#myTable tr").filter(function() {
                    $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
                });
            });
        });
    </script>


@endsection
@section('content')
    <div class="card">

        <div class="card-header">
            <h3 class="card-title" style="font-family: 'B Yekan';text-align: right;"><i class="fa fa-list-alt" style="position: relative;top: 2px;left: 4px;"></i>لیست زیرمنو</h3>

            <a href="\admin\report\create"><button type="button" class="bg bg-success" style="border-radius: 5px;"> <i class="fa fa-plus" aria-hidden="true"></i></button></a>



        </div>
        <!-- /.card-header -->

        <div class="card-body">
            <div class="row">

                <div class="col-lg-6" style="float: left!important;">
                    <input class="form-control" id="myInput" type="text" placeholder="جستجو...." style="margin-bottom: 20px;font-family: 'B Yekan'">
                </div>
            </div>
            <?php $tbl=DB::table('subsystem')->get(); ?>
            @if(count($tbl)>=1)
                <table  class="table table-bordered table-striped js-sort-table"  id="demo1">
                    <thead>
                    <tr>
                        <th>کدزیرمنو</th>
                        <th>عنوان زیرمنو</th>
                        <th>عنوان خارجی زیرمنو</th>
                        <th>منو وابسته</th>
                        <th>لینک صفحه</th>
                        <td>ایکون زیرمنو</td>
                        <th>وضعیت</th>
                        <th>عملیات</th>
                    </tr>
                    </thead>

                    <tbody id="myTable" class="sortable">
                    @foreach($submenuu as $s)
                    <tr>
                    <td>{{$s->id}}</td>
                    <td>{{$s->title}}</td>
                    <td>{{$s->title_en}}</td>
                    <td>{{$s->titleme}}</td>
                    <td>{{$s->route}}</td>
                    <td><i class="{{$s->icon_class}}"></i></td>
                    <td>
                    @if($s->status==true)
                    <span class="badge badge-pill badge-success">فعال</span>
                    @elseif($s->status==false)
                    <span class="badge badge-pill badge-danger">غیرفعال</span>
                    @endif
                    </td>
                    <td style="width:17%">
                    <form action="{{route('submenu.destroy',$s->id)}}" id="closee" method="post" style="width: 2%;">
                    {{@method_field('delete')}}
                    {{@csrf_field()}}
                    <button type="submit" class="btn btn-danger btnaa" style="border-radius: 5px">
                    <i class="fa fa-trash-o"></i>
                    </button>
                    </form>
                    <form  action="{{route('submenu.edit',$s->id)}}"  style="width: 2%;">
                    <button type="submit" class="btn btn-success btnab" style="border-radius: 5px;">
                    <i class="fa fa-edit"></i>
                    </button>
                    </form>
                    @if($s->status==true)
                    <form  action="/admin/disablesub/" method="post"  style="width: 2%;">
                    {{@csrf_field()}}
                    {{@method_field('patch')}}
                    <input type="hidden" name="disable_id" value="{{$s->id}}">
                    <button type="submit" class="btn btn-primary btnac" style="border-radius: 5px;">
                    <i class="fa fa-check-circle-o"></i>
                    </button>
                    </form>

                    @elseif($s->status==false)
                    <form  action="/admin/enablesub/" method="post"  style="width: 2%;">
                    {{@csrf_field()}}
                    {{@method_field('patch')}}
                    <input type="hidden" name="enable_id" value="{{$s->id}}">
                    <button type="submit" class="btn btn-success btnac" style="border-radius: 5px;">
                    <i class="fa fa-check-circle-o"></i>
                    </button>
                    </form>

                    @endif
                    <form  action="/admin/submenu/show" method="get"  style="width: 2%;">
                            {{@csrf_field()}}
                            {{@method_field('get')}}
                            <input type="hidden" name="submenu_id" value="{{$s->id}}">
                            <button type="submit" class="btn btn-success btnac" style="border-radius: 5px;position: relative;top: -85px;right: 139px;">
                                <i class="fa fa-eye"></i>
                            </button>
                    </form>
                    </td>
                    </tr>

                    @endforeach
                    </tbody>
                    <tfoot>
                    <tr>
                        <th>کدزیرمنو</th>
                        <th>عنوان زیرمنو</th>
                        <th>عنوان خارجی زیرمنو</th>
                        <th>منو وابسته</th>
                        <th>لینک صفحه</th>
                        <td>ایکون زیرمنو</td>
                        <th>وضعیت</th>
                        <th>عملیات</th>
                    </tr>
                    </tfoot>
                </table>
            @else
                <p style="font-family: 'B Yekan'">چیزی برای نمایش وجود ندارد</p>
            @endif
        </div>
        <ul class="pagination">
            {{$submenuu->links()}}
        </ul>
        <!-- /.card-body -->
    </div>

@stop

