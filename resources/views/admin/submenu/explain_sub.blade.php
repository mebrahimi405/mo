@extends('layout.index')
@section('content')

    <div class="row" style="width: 100%;height: 100%;position: relative;right:16px;font-family: 'B Yekan';font-size:20px;color: #000;">
        <div class="col-lg-12 bg-white">
            <p style="text-align: right;padding-right: 8px;padding-top:8px;box-sizing: border-box">جزئیات زیر منو :{{$sub->title}}</p>
            <hr>
            <div class="col-lg-6 float-right" style="padding-top: 20px;box-sizing: border-box">
                <label for="">عنوان زیر منو:{{$sub->title}}</label>
            </div>
            <div class="col-lg-6 float-left" style="padding-top: 20px;box-sizing: border-box">
                <label for="">عنوان خارجی  زیر منو:{{$sub->title_en}}</label>
            </div>
            <div class="col-lg-6 float-right" style="padding-top: 20px;box-sizing: border-box">
                <label for="">لینک  زیر منو:{{$sub->route}}</label>
            </div>
            <div class="col-lg-6 float-left" style="padding-top: 20px;box-sizing: border-box">
                <label for="">ایکون  زیر منو:  <i class="{{$sub->icon_class}}"></i></label>
            </div>
            <div class="col-lg-6 float-left" style="padding-top: 20px;box-sizing: border-box">
                <form action="\admin\menu\" style="position:relative;right:400px;top: 50px;">
                    <button class="btn btn-primary" style="border-radius: 5px">بازگشت</button>
                </form>
            </div>
        </div>
    </div>
@endsection