@extends('layout.index')

@section('content')

    <!-- Buttons to choose list or grid view -->
    <button onclick="listView()"><i class="fa fa-bars"></i> لیست</button>
    <button onclick="gridView()"><i class="fa fa-th-large"></i> گرید</button>
    {{Form::open(array('action'=>'Administrator\sliderController@save','files'=>true))}}
    <form action="" method="post" enctype="multipart/form-data">
        <input type="file" name="img">
        <button type="submit" class="btn btn-success">
            <i class="fa fa-image"><span style="margin-right: 8px">ارسال</span></i>
        </button>
    </form>
    <div class="row">
        @foreach($gallery as $slide)
        <div class="column">
            <img src="../../images/slide/{{$slide->img}}" alt="" width="100px" height="100px">
        </div>
            @endforeach
    </div>


{{Form::close()}}
@endsection