@extends('layout.home')
@section('postbox')
    <div class="postbox">
        <!--post-->
        <div class="shortstory">
            <img src="../images/109603_997.jpg" width="60" />
            <b><a href="/">يك نوع نفت خام جديد در سبد عرضه نفتی ایران </a></b><br />
            در صورت فراهم شدن زیرساخت لازم، تعریف نفت خام جدید در سبد عرضه نفتی کشور از لحاظ اقتصادی، بسیار به صرفه و درآمدزا خواهد بود، زیرا طبیعی است که ما نفت
            <div class="cls"></div>
        </div>
        <!--end_post-->
        <!--post-->
        <div class="shortstory">
            <img src="../images/109902_540.jpg" width="60" />
            <b><a href="/">این سخنان بوی اومانیسم می‌دهد...  </a></b><br />
            در صورت فراهم شدن زیرساخت لازم، تعریف نفت خام جدید در سبد عرضه نفتی کشور از لحاظ اقتصادی، بسیار به صرفه و درآمدزا خواهد بود، زیرا طبیعی است که ما نفت
            <div class="cls"></div>
        </div>
        <!--end_post-->
        <!--post-->
        <div class="shortstory">
            <img src="../images/109986_192.jpg" width="60" />
            <b><a href="/">افزودن بر تعداد وعده‌هاي بزرگ به سود كيست؟  </a></b><br />
            در صورت فراهم شدن زیرساخت لازم، تعریف نفت خام جدید در سبد عرضه نفتی کشور از لحاظ اقتصادی، بسیار به صرفه و درآمدزا خواهد بود، زیرا طبیعی است که ما نفت
            <div class="cls"></div>
        </div>
        <!--end_post-->
        <!--post-->
        <div class="shortstory">
            <img src="../images/109988_396.jpg" width="60" />
            <b><a href="/">جزييات بيشتر از اختلاس 3 هزار ميلياردي </a></b><br />
            در صورت فراهم شدن زیرساخت لازم، تعریف نفت خام جدید در سبد عرضه نفتی کشور از لحاظ اقتصادی، بسیار به صرفه و درآمدزا خواهد بود، زیرا طبیعی است که ما نفت
            <div class="cls"></div>
        </div>
        <!--end_post-->
        <!--post-->
        <div class="shortstory">
            <img src="../images/109991_841.jpg" width="60" />
            <b><a href="/">می گویند «مطلقه» یعنی اداره کشور بر اساس میل شخصی اما... </a></b><br />
            در صورت فراهم شدن زیرساخت لازم، تعریف نفت خام جدید در سبد عرضه نفتی کشور از لحاظ اقتصادی، بسیار به صرفه و درآمدزا خواهد بود، زیرا طبیعی است که ما نفت
            <div class="cls"></div>
        </div>
        <!--end_post-->
        <!--post-->
        <div class="shortstory">
            <img src="../images/109835_297.jpg" width="60" />
            <b><a href="/">«شیر دره پنجشیر» فراموش شدنی نیست  </a></b><br />
            در صورت فراهم شدن زیرساخت لازم، تعریف نفت خام جدید در سبد عرضه نفتی کشور از لحاظ اقتصادی، بسیار به صرفه و درآمدزا خواهد بود، زیرا طبیعی است که ما نفت
            <div class="cls"></div>
        </div>
        <!--end_post-->
    </div>
@stop
@section('box-right')
    <div class="box_right">

        <!-- jquery - tab -->
        <table border="0" cellpadding="0" cellspacing="0" class="pageme">

            <tr>
                <td>
                    <div class="title_more0" style="background:url(img/img_01-1.jpg)"><b>آخرین اخبار ورزشی</b></div>
                    <div class="pad">

                        <ul class="box_news_i">
                            <li>
                                <img src="../images/n00134155-s.jpg" width="60" height="45"/>
                                متاسفانه با پیگیری کمیته حمایت از انقلاب اسلامی مردم فلسطین زیر ...
                            </li>

                            <li>
                                <img src="../images/n00134155-s.jpg" width="60" height="45" />
                                متاسفانه با پیگیری کمیته حمایت از انقلاب اسلامی مردم فلسطین زیر ...
                            </li>

                            <li>
                                <img src="../images/n00134155-s.jpg" width="60" height="45" />
                                متاسفانه با پیگیری کمیته حمایت از انقلاب اسلامی مردم فلسطین زیر ...
                            </li>
                        </ul>

                    </div>
                </td>
            </tr>

            <tr>
                <td>
                    <div class="title_more0" style="background:url(img/img_01-2.jpg)"><b>آخرین اخبار سیاسی</b></div>
                    <div class="pad">

                        <ul class="box_news_i">
                            <li>
                                <img src="../images/n00185209-t.jpg" width="60" height="45" />
                                متاسفانه با پیگیری کمیته حمایت از انقلاب اسلامی مردم فلسطین زیر ...
                            </li>

                            <li>
                                <img src="../images/n00185209-t.jpg" width="60" height="45" />
                                متاسفانه با پیگیری کمیته حمایت از انقلاب اسلامی مردم فلسطین زیر ...
                            </li>

                            <li>
                                <img src="../images/n00185209-t.jpg" width="60" height="45" />
                                متاسفانه با پیگیری کمیته حمایت از انقلاب اسلامی مردم فلسطین زیر ...
                            </li>
                        </ul>

                    </div>

                </td>
            </tr>

            <tr>
                <td>
                    <div class="title_more0" style="background:url(img/img_01-3.jpg)"><b>آخرین اخبار اقتصادی</b></div>
                    <div class="pad">

                        <ul class="box_news_i">
                            <li>
                                <img src="../images/n00134155-s.jpg" width="60" />
                                متاسفانه با پیگیری کمیته حمایت از انقلاب اسلامی مردم فلسطین زیر ...
                            </li>

                            <li>
                                <img src="../images/n00134155-s.jpg" width="60" />
                                متاسفانه با پیگیری کمیته حمایت از انقلاب اسلامی مردم فلسطین زیر ...
                            </li>

                            <li>
                                <img src="../images/n00134155-s.jpg" width="60" />
                                متاسفانه با پیگیری کمیته حمایت از انقلاب اسلامی مردم فلسطین زیر ...
                            </li>
                        </ul>

                    </div>

                </td>
            </tr>


            <tr>
                <td>
                    <div class="title_more0" style="background:url(img/img_01-4.jpg)"><b>آخرین اخبار حوادث</b></div>
                    <div class="pad">

                        <ul class="box_news_i">
                            <li>
                                <img src="../images/n00134155-s.jpg" width="60" />
                                متاسفانه با پیگیری کمیته حمایت از انقلاب اسلامی مردم فلسطین زیر ...
                            </li>

                            <li>
                                <img src="../images/n00134155-s.jpg" width="60" />
                                متاسفانه با پیگیری کمیته حمایت از انقلاب اسلامی مردم فلسطین زیر ...
                            </li>

                            <li>
                                <img src="../images/n00134155-s.jpg" width="60" />
                                متاسفانه با پیگیری کمیته حمایت از انقلاب اسلامی مردم فلسطین زیر ...
                            </li>
                        </ul>

                    </div>

                </td>
            </tr>

        </table>
        <div class="pager"></div>
        <div class="cls"></div>
        <!-- jquery - tab-end -->




        <div class="title_more"><b>مروری بر اخبار روز</b></div>
        <div class="pad">

            <ul class="box_news_i">
                <li>
                    <img src="../images/n00134155-s.jpg" width="60" />
                    متاسفانه با پیگیری کمیته حمایت از انقلاب اسلامی مردم فلسطین زیر ...
                </li>

                <li>
                    <img src="../images/n00134155-s.jpg" width="60" />
                    متاسفانه با پیگیری کمیته حمایت از انقلاب اسلامی مردم فلسطین زیر ...
                </li>

                <li>
                    <img src="../images/n00134155-s.jpg" width="60" />
                    متاسفانه با پیگیری کمیته حمایت از انقلاب اسلامی مردم فلسطین زیر ...
                </li>
            </ul>

        </div>

        <div class="title_more"><b>مروری بر اخبار روز</b></div>
        <div class="pad">

            <ul class="link_box1">
                <li><a href="/"> واكنش صادق طباطبایی به خبر كشف پیكر امام موسی صدر </a></li>
                <li><a href="/"> ایان حبس ۷۰ ساله اسناد اشغال ایران از سوی متفقین </a></li>
                <li><a href="/"> عکسی از دیدار مصباح‌یزدی با آیت‌الله بهجت </a></li>
                <li><a href="/"> تهدید مسعود فراستی از سوی یک سینماگر: با ماشین از رویت رد می‌شوم </a></li>
                <li><a href="/"> پست جدید مدیرعامل سابق فارس در سپاه </a></li>
                <li><a href="/"> عکسی از دیدار مصباح‌یزدی با آیت‌الله بهجت </a></li>
                <li><a href="/"> علی مطهری: اظهارات احمدی‌نژاد مرا یاد خاتمی انداخت </a></li>
            </ul>

        </div>
    </div>
@endsection