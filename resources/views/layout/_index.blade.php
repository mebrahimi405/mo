<!--
 * CoreUI - Open Source Bootstrap Admin Template
 * @version v1.0.0-alpha.6
 * @link http://coreui.io
 * Copyright (c) 2017 creativeLabs Łukasz Holeczek
 * @license MIT
 -->
<!DOCTYPE html>
<html lang="fa"  dir="rtl">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="CoreUI - Open Source Bootstrap Admin Template">
    <meta name="author" content="Łukasz Holeczek">





    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="stylesheet" type="text/css" media="screen" href="../../../phpgrid/themes/redmond/jquery-ui.custom.css"></link>

    <link rel="stylesheet" type="text/css" media="screen" href="../../../phpgrid/jqgrid/css/ui.jqgrid.bs.css"></link>
    <script src="../../../phpgrid/jquery.min.js" type="text/javascript"></script>
    <script src="../../../phpgrid/grid.locale-fa.js" type="text/javascript"></script>
    <script src="../../../phpgrid/jqgrid/js/jquery.jqGrid.min.js" type="text/javascript"></script>
    <script src="../../../phpgrid/themes/jquery-ui.custom.min.js" type="text/javascript"></script>


    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    @yield('css')
    <link href="<?php echo url('../css/fontawesome-iconpicker.css') ?>" rel="stylesheet">
    <link href="<?php echo url('../css/fontawesome-iconpicker.min.css') ?>" rel="stylesheet">
    <script src="<?php echo url('../js/fontawesome-iconpicker.js')?>" type="text/javascript"></script>
    <script src="<?php echo url('../js/fontawesome-iconpicker.min.js')?>" type="text/javascript"></script>
    <script src="<?php echo url('../js/fontawesome-iconpicker.js')?>"></script>
    <script src="<?php echo url('../js/iconpicker.js')?>"></script>
    <link rel="stylesheet" type="text/css" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css">

    <script src="<?php echo url('../js/jquery.ui.pos.js')?>"></script>
    <script src="<?php echo url('../js/popup.js')?>"></script>

    <!-- Bootstrap-Iconpicker -->

    <!-- Bootstrap-Iconpicker Bundle -->
    <script type="text/javascript" src="<?php echo url('../js/bootstrap-iconpicker.bundle.min.js')?>"></script>
    <meta name="keyword" content="Bootstrap,Admin,Template,Open,Source,AngularJS,Angular,Angular2,Angular 2,Angular4,Angular 4,jQuery,CSS,HTML,RWD,Dashboard,React,React.js,Vue,Vue.js">
    <link rel="shortcut icon" href="img/favicon.png">
    <title>پنل مدیریت</title>

    <!-- Icons -->
    <link href="<?php echo url('../css/font-awesome.css')?>" rel="stylesheet">
    <link href="<?php echo url('../css/simple-line-icons.css')?>" rel="stylesheet">
    <!-- Main styles for this application -->
    <link href="<?php echo url('../css/stylee.css');?>" rel="stylesheet">
   {{-- <script type="text/javascript">
        // Get the elements with class="column"
        var elements = document.getElementsByClassName("column");

        // Declare a loop variable
        var i;

        // List View
        function listView() {
            for (i = 0; i < elements.length; i++) {
                elements[i].style.width = "100%";
            }
        }

        // Grid View
        function gridView() {
            for (i = 0; i < elements.length; i++) {
                elements[i].style.width = "50%";
            }
        }


    </script>--}}
    <script type="text/javascript">

    </script>
</head>

<body class="app header-fixed sidebar-fixed aside-menu-fixed aside-menu-hidden">
<header class="app-header navbar">
    <button class="navbar-toggler mobile-sidebar-toggler d-lg-none mr-auto" type="button">☰</button>
    <a class="navbar-brand" href="#"></a>
    <button class="navbar-toggler sidebar-minimizer d-md-down-none" type="button">☰</button>

    <ul class="nav navbar-nav d-md-down-none">
        <li class="nav-item px-3">
            <a class="nav-link" href="/admin/panel">میزکار</a>
        </li>
        <li class="nav-item px-3">
            <a class="nav-link" href="/admin/user">کاربران</a>
        </li>
        <li class="nav-item px-3">
            <a class="nav-link" href="#">تنظیمات</a>
        </li>
    </ul>
    <ul class="nav navbar-nav ml-auto">
        <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle nav-link" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                <img src="../../images/pic/{{Auth::user()->picture}}" class="img-avatar" alt="تصویرکاربر">
                <span class="d-md-down-none">سلام {{auth::user()->name}}</span>
            </a>
            <div class="dropdown-menu dropdown-menu-right">
                <div class="dropdown-header text-center">
                    <strong>حساب کاربری</strong>
                </div>
                <a class="dropdown-item" href="/admin/comment"><i class="fa fa-comments"></i> نظرات<span class="badge badge-warning"></span></a>
                <div class="dropdown-header text-center">
                    <strong>تنظیمات</strong>
                </div>
                <a class="dropdown-item" href="\admin\profile\"><i class="fa fa-info"></i> پروفایل</a>

                <a class="dropdown-item" href="{{ route('logout') }}"
                   onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                    <i class="fa fa-sign-out" aria-hidden="true"></i>  {{ __('خروج') }}
                </a>

                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
            </div>
        </li>
    </ul>

</header>


    <!-- Main content -->
    <main class="main">
        @yield('content')
    </main>

</div>

<footer class="app-footer ltr">
    <a href="http://iracode.com">
        <span class="float-left">Powered by iracode</span></a>
    </span>
</footer>{{--
<script src="<?php echo url('../js/jquery.js')?>"></script>--}}
<script src="<?php echo url('../js/popper.min.js')?>"></script>
<script src="<?php echo url('../js/bootstrap.min.js')?>"></script>
<script src="<?php echo url('../js/pace.js')?>"></script>
<script type="text/javascript">
    swal({
        buttons: {
            cancel: true,
            confirm: "Confirm",
            roll: {
                text: "Do a barrell roll!",
                value: "roll",
            },
        },
    });
</script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/8.11.8/sweetalert2.all.js"></script>
@include('sweetalert::alert')


<!-- GenesisUI main scripts -->

<script src="<?php echo url('../js/papp.js')?>"></script>
<script src="<?php echo url('../js/pmain.js')?>"></script>

<script src="//cdn.ckeditor.com/4.13.0/full/ckeditor.js"></script>
<script src="/rofil-ckeditor/laravel-ckeditor/adapters/jquery.js"></script>
@yield('script')


<!-- Plugins and scripts required by this views -->

<!-- Custom scripts required by this view -->
<script src="js/views/main.js"></script>

</body>

</html>