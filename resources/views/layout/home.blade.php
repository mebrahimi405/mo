<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>پیش نمایش قالب خبری</title>
</head>

<script src="http://code.jquery.com/jquery-latest.js"></script>
<script type="text/javascript" src="js/quickpager.jquery.js"></script>
<script src="<?php echo url('../js/doc.js')?>"></script>
<link rel="stylesheet" href="<?php echo url('../css/style.css')?>">
<link rel="canonical" href="http://www.20script.ir" />
<link rel="canonical" href="http://www.bistscript.ir" />
<script type="text/javascript">
    $(document).ready(function() {
        $(".pageme tbody").quickPager( {
            pageSize: 1,
            currentPage: 1,
            holder: ".pager"
        });
    });
</script>




<body>


<div id="message"><div class="massage_box">
        <a href="/">صفحه اصلی</a> |
        <a href="/">فن آوری</a> |
        <a href="/">مذهبی</a> |
        <a href="/">هنری</a> |
        <a href="/">ورزشی</a> |
        <a href="/">اقتصادی</a> |
        <a href="/">مقاله</a> |
        <a href="/">محیط زیست</a> |
        <a href="/">فرهنگی</a> |
        <a href="/">اجتماعی</a> |
        <a href="/">رسانه</a> |
        <a href="/">دولت</a> |
        <a href="/">سیاسی</a>
    </div></div>


<div class="loading"><p>
        <img src="img/stu.gif" /><br />
        لطفا چند لحظه صبر کنید .
    </p></div>

<!--هدر سایت-->
<div class="header">

    <div class="logo"></div>

    <ul class="ico_top">
        <li><img src="../images/feedback.png" /></li>
        <li><img src="../images/rss.png" /></li>
        <li><img src="../images/home.png" /></li>
    </ul>

    <div class="link_bar"><a href="/">» قحطی زدگان سومالی، چگونه با لب‌های تشنه سر به سجده می‌گذارند  ... به صورت تایپی</a></div>

    <form action="" method="post">
        <div class="search">
            <input type="text" name="search" value="جستجو در آرشیو اخبار ..." onfocus="if(this.value == 'جستجو در آرشیو اخبار ...')this.value=''" onblur="if(this.value == '')this.value='جستجو در سایت ...'" />
        </div>
        <div class="search_submit"><button type="submit" name="submit"></button></div>
    </form>

</div>

<!-- محتوای سایت -->
<div class="body_box"><div class="body_box_bg">

        <div class="box_left">
            <ul class="news_list">
                <a href="/"><li title="فن آوری" class="i4 tooltip"></li></a>
                <a href="/"><li title="مذهبی" class="i5 tooltip"></li></a>
                <a href="/"><li title="هنری" class="i4 tooltip"></li></a>
                <a href="/"><li title="ورزشی" class="i3 tooltip"></li></a>
                <a href="/"><li title="حوادث" class="i3 tooltip"></li></a>
                <a href="/"><li title="اقتصادی" class="i4 tooltip"></li></a>
                <a href="/"><li title="مقاله" class="i3 tooltip"></li></a>
                <a href="/"><li title="محیط زیست" class="i3 tooltip"></li></a>
                <a href="/"><li title="فرهنگی" class="i3 tooltip"></li></a>
                <a href="/"><li title="اجتماعی" class="i3 tooltip"></li></a>
                <a href="/"><li title="رسانه ها" class="i3 tooltip"></li></a>
                <a href="/"><li title="دولت" class="i3 tooltip"></li></a>
                <a href="/"><li title="سیاسی" class="i3 tooltip"></li></a>
            </ul>

            <div class="top_box_news">

                <div class="top_box_news_right">

                    <div id="slide1">
                        <img src="../images/n00185162-t.jpg" width="146" height="110" />
                        <b><a href="/">انسان می‌تواند همه مخلوقات خدا را خلق ...</a></b><br />
                        رئیس جمهور با تاکید بر اینکه خدای متعال راه کسب همه علوم را در برابر انسان باز کرده است، گفت:
                        انسان تنها قدرت دستیابی به ذات خداوند را ندارد والا همه آنچه خداوند خلق کرده انسان هم می تواند خلق کند. ...
                    </div>

                    <div id="slide2">
                        <img src="../images/n00185209-t.jpg" width="146" height="110" />
                        <b><a href="/">تمدن: قسم جلاله مي‌خورم تخلف نكرديم</a></b><br />
                        مرتضي تمدن گفت كه در صورت اثبات ادعاي اختلاس در استانداري تهران كه اين روزها مطرح شده، حق وي و همكارانش محاكمه است.
                        خبرگزاری فارس: تمدن: قسم جلاله مي‌خورم تخلف نكرديم ...
                    </div>


                </div>

                <div class="top_box_news_left">
                    <p>عناوین مهم خبری روز</p>
                    <ul class="top_news">
                        <li><a href="/">هشدار به صاحبان قدرت و دستگاهاي اجرائي كه درمقابل با مبارزه با فساد مانع تراشي مي كنند</a></li>
                        <li><a href="/">میرحسین به حضور در انتخابات مجلس امیدوار است!</a></li>
                        <li><a href="/">انسان می‌تواند همه مخلوقات خدا را خلق کند</a></li>
                    </ul>
                </div>
                <div class="cls"></div>
            </div>





            <div class="body_text">

                <div class="ads">
                    <OBJECT data="http://www.tabnak.ir/files/adv/2610_239.swf" type="application/x-shockwave-flash" width="130" height="140">
                        <PARAM NAME=movie VALUE="http://www.tabnak.ir/files/adv/2610_239.swf">
                        <PARAM NAME=menu VALUE=false>
                    </OBJECT>

                    <OBJECT data="http://www.tabnak.ir//files/adv/2622_488.swf" type="application/x-shockwave-flash" width="130" height="140">
                        <PARAM NAME=movie VALUE="http://www.tabnak.ir//files/adv/2622_488.swf">
                        <PARAM NAME=menu VALUE=false>
                    </OBJECT>

                    <OBJECT data="http://www.tabnak.ir/files/adv/2637_887.swf" type="application/x-shockwave-flash" width="130" height="210">
                        <PARAM NAME=movie VALUE="http://www.tabnak.ir/files/adv/2637_887.swf">
                        <PARAM NAME=menu VALUE=false>
                    </OBJECT>
                </div>

                <div class="linkdoni">
                    <div class="title_linkdoni"><b> عناوین خبری</b></div>
                    <ul class="link_box2">
                        <li><a href="/"> واكنش صادق طباطبایی به خبر كشف پیكر امام موسی صدر </a></li>
                        <li><a href="/"> ایان حبس ۷۰ ساله اسناد اشغال ایران از سوی متفقین </a></li>
                        <li><a href="/"> تهدید مسعود فراستی از سوی یک سینماگر: با ماشین از رویت رد می‌شوم </a></li>
                        <li><a href="/"> پست جدید مدیرعامل سابق فارس در سپاه </a></li>
                        <li><a href="/"> تهدید مسعود فراستی از سوی یک سینماگر: با ماشین از رویت رد می‌شوم </a></li>
                        <li><a href="/"> پست جدید مدیرعامل سابق فارس در سپاه </a></li>
                        <li><a href="/"> عکسی از دیدار مصباح‌یزدی با آیت‌الله بهجت </a></li>
                        <li><a href="/"> علی مطهری: اظهارات احمدی‌نژاد مرا یاد خاتمی انداخت </a></li>
                        <li><a href="/"> واكنش صادق طباطبایی به خبر كشف پیكر امام موسی صدر </a></li>
                        <li><a href="/"> ایان حبس ۷۰ ساله اسناد اشغال ایران از سوی متفقین </a></li>
                        <li><a href="/"> تهدید مسعود فراستی از سوی یک سینماگر: با ماشین از رویت رد می‌شوم </a></li>
                        <li><a href="/"> پست جدید مدیرعامل سابق فارس در سپاه </a></li>
                    </ul>
                </div>



                @yield('postbox')


                <div class="cls"></div>
            </div>






        </div>





            @yield('box-right')






        <div class="cls"></div>
    </div></div>

<!--footer-->
<div class="footer">
    <div class="footer_bg">

        <p>تمامی حقوق این سایت متعلق به پورتال خبری ساوجی ها می باشد.<Br /> طراحی و برنامه نویسی توسط : 20Script.ir | بیست اسکریپت </p>
    </div></div>



<!-- GotoTop by www.1abzar.com --->
<!-- GotoTop by www.1abzar.com --->

</body>

<div style="visibility:hidden;display:none">
    <a href="http://www.20script.ir">http://www.20script.ir</a>
    <div>
</html>
