﻿<!--
 * CoreUI - Open Source Bootstrap Admin Template
 * @version v1.0.0-alpha.6
 * @link http://coreui.io
 * Copyright (c) 2017 creativeLabs Łukasz Holeczek
 * @license MIT
 -->
<!DOCTYPE html>
<html lang="fa"  dir="rtl">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="CoreUI - Open Source Bootstrap Admin Template">
    <meta name="author" content="Łukasz Holeczek">
    <title>پنل مدیریت</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    @yield('css')
    <link href="<?php use Illuminate\Support\Facades\Auth;echo url('../css/fontawesome-iconpicker.css') ?>" rel="stylesheet">
    <link href="<?php echo url('../css/fontawesome-iconpicker.min.css') ?>" rel="stylesheet">
    <script src="<?php echo url('../js/fontawesome-iconpicker.js')?>" type="text/javascript"></script>
    <script src="<?php echo url('../js/fontawesome-iconpicker.min.js')?>" type="text/javascript"></script>
    <script src="<?php echo url('../js/fontawesome-iconpicker.js')?>"></script>
    <script src="<?php echo url('../css/dataTables.bootstrap4.css')?>"></script>
    <script src="<?php echo url('../css/jquery.dataTables.css')?>"></script>
    <script src="<?php echo url('../js/iconpicker.js')?>"></script>
    <link rel="stylesheet" type="text/css" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css">
    <meta charset='utf-8' />


    <!-- Bootstrap-Iconpicker -->

    <!-- Bootstrap-Iconpicker Bundle -->
    <script type="text/javascript" src="<?php echo url('../js/bootstrap-iconpicker.bundle.min.js')?>"></script>
    <!-- Icons -->
    <link href="<?php echo url('../css/font-awesome.css')?>" rel="stylesheet">
    <!-- Main styles for this application -->
    <link href="<?php echo url('../css/stylee.css');?>" rel="stylesheet">
    <script src="../../plugins/jquery/jquery.min.js"></script>
    <!-- Bootstrap 4 -->
    <!-- DataTables -->


</head>

<!-- BODY options, add following classes to body to change options

// Header options
1. '.header-fixed'					- Fixed Header

// Sidebar options
1. '.sidebar-fixed'					- Fixed Sidebar
2. '.sidebar-hidden'				- Hidden Sidebar
3. '.sidebar-off-canvas'		- Off Canvas Sidebar
4. '.sidebar-minimized'			- Minimized Sidebar (Only icons)
5. '.sidebar-compact'			  - Compact Sidebar

// Aside options
1. '.aside-menu-fixed'			- Fixed Aside Menu
2. '.aside-menu-hidden'			- Hidden Aside Menu
3. '.aside-menu-off-canvas'	- Off Canvas Aside Menu

// Breadcrumb options
1. '.breadcrumb-fixed'			- Fixed Breadcrumb

// Footer options
1. '.footer-fixed'					- Fixed footer

-->

<body class="app header-fixed sidebar-fixed aside-menu-fixed aside-menu-hidden">
<header class="app-header navbar">
    <button class="navbar-toggler mobile-sidebar-toggler d-lg-none mr-auto" type="button">☰</button>
    <a class="navbar-brand" href="#"></a>
    <button class="navbar-toggler sidebar-minimizer d-md-down-none" type="button">☰</button>

    <ul class="nav navbar-nav d-md-down-none">
        <li class="nav-item px-3">
            <a class="nav-link" href="/admin/panel">میزکار</a>
        </li>
        <li class="nav-item px-3">
            <a class="nav-link" href="/admin/user">کاربران</a>
        </li>
        <li class="nav-item px-3">
            <a class="nav-link" href="#">تنظیمات</a>
        </li>
    </ul>
    <ul class="nav navbar-nav ml-auto">
        <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle nav-link" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                <img src="../../images/pic/{{Auth::user()->picture}}" class="img-avatar" alt="تصویرکاربر">
                <span class="d-md-down-none">سلام {{auth::user()->name}}</span>
            </a>
            <div class="dropdown-menu dropdown-menu-right">
                <div class="dropdown-header text-center">
                    <strong>حساب کاربری</strong>
                </div>
                <a class="dropdown-item" href="/admin/comment"><i class="fa fa-comments"></i> نظرات<span class="badge badge-warning"></span></a>
                <div class="dropdown-header text-center">
                    <strong>تنظیمات</strong>
                </div>
                <a class="dropdown-item" href="\admin\profile\"><i class="fa fa-info"></i> پروفایل</a>

                <a class="dropdown-item" href="{{ route('logout') }}"
                   onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                    <i class="fa fa-sign-out" aria-hidden="true"></i>  {{ __('خروج') }}
                </a>

                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
            </div>
        </li>
    </ul>

</header>

<div class="app-body">
    <div class="sidebar">
        <nav class="sidebar-nav">
            <ul class="nav" style="font-family: 'B titr'">

                <?php
                $menu=DB::table('menu')->get();
                ?>

                    @foreach($menu as $mn)
                        <?php ?>
                        @if($mn->status==1)
                <li class="nav-item nav-dropdown">
                    <?php $id=$mn->id;?>
                    <?php
                    $u=Auth::user()->id;
                    $item_name=DB::table('auth_assignment')->select('item_name')->where("user_id","=",$u)->first()->item_name;
                    $menu_id=DB::table('auth_menu')->where("item_name","=",$item_name)->get();
                    $mew="";
                    foreach ($menu_id as $m_id):
                        $mew.= $m_id->menu_id.',';

                    endforeach;
                    $mw=rtrim($mew,',');
                    $arr = explode(',',$mew);
                    ?>
                    @if(in_array($id,$arr))
                        <a class="nav-link nav-dropdown-toggle" ><i class="{{$mn->icon_classme}}"></i>{{$mn->titleme}}</a>
                    @endif
                        <?php

                        ?>
                    <?php   $submenu=DB::table('subsystem')->join('menu','menu.id','=','subsystem.menu_id')->get();
                     ?>
                    @foreach($submenu as $mq)
                        @if($id==$mq->menu_id)

                        <ul class="">
                            <li class="nav-item" >

                                <a class="nav-link" href="/{{$mq->route}}"><i class="{{$mq->icon_class}}"></i>{{$mq->title}}</a>
                            </li>

                        </ul>

                            @endif
                    @endforeach
                        @endif

                </li>
                    @endforeach

            </ul>
        </nav>
    </div>

    <!-- Main content -->
    <main class="main">
    @yield('content')
    </main>
    <aside class="aside-menu">
        <ul class="nav nav-tabs" role="tablist">
            <li class="nav-item">
                <a class="nav-link active" data-toggle="tab" href="#timeline" role="tab"><i class="icon-list"></i></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#messages" role="tab"><i class="icon-speech"></i></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#settings" role="tab"><i class="icon-settings"></i></a>
            </li>
        </ul>

        <!-- Tab panes -->
        <div class="tab-content">
            <div class="tab-pane active" id="timeline" role="tabpanel">
                <div class="callout m-0 py-2 text-muted text-center bg-light text-uppercase">
                    <small><b>امروز</b>
                    </small>
                </div>
                <hr class="transparent mx-3 my-0">
                <div class="callout callout-warning m-0 py-3">
                    <div class="avatar float-left">
                        <img src="img/avatars/7.jpg" class="img-avatar" alt="admin@bootstrapmaster.com">
                    </div>
                    <div>ملاقات با
                        <strong>فرشید</strong>
                    </div>
                    <small class="text-muted mr-3"><i class="icon-calendar"></i>&nbsp; 1 - 3pm</small>
                    <small class="text-muted"><i class="icon-location-pin"></i>&nbsp; Palo Alto, CA</small>
                </div>
                <hr class="mx-3 my-0">
                <div class="callout callout-info m-0 py-3">
                    <div class="avatar float-left">
                        <img src="img/avatars/4.jpg" class="img-avatar" alt="admin@bootstrapmaster.com">
                    </div>
                    <div>اسکایپ با
                        <strong>مهسا</strong>
                    </div>
                    <small class="text-muted mr-3"><i class="icon-calendar"></i>&nbsp; 4 - 5pm</small>
                    <small class="text-muted"><i class="icon-social-skype"></i>&nbsp; On-line</small>
                </div>
                <hr class="transparent mx-3 my-0">
                <div class="callout m-0 py-2 text-muted text-center bg-light text-uppercase">
                    <small><b>فردا</b>
                    </small>
                </div>
                <hr class="transparent mx-3 my-0">
                <div class="callout callout-danger m-0 py-3">
                    <div>پروژه گرافیکی -
                        <strong>زمان بندی</strong>
                    </div>
                    <small class="text-muted mr-3"><i class="icon-calendar"></i>&nbsp; 10 - 11pm</small>
                    <small class="text-muted"><i class="icon-home"></i>&nbsp; creativeLabs HQ</small>
                    <div class="avatars-stack mt-2">

                        <div class="avatar avatar-xs">

                        </div>
                    </div>
                </div>
                <hr class="mx-3 my-0">
                <div class="callout callout-success m-0 py-3">
                    <div>
                        <strong>#10 استارتاپ</strong>جلسه</div>
                    <small class="text-muted mr-3"><i class="icon-calendar"></i>&nbsp; 1 - 3pm</small>
                    <small class="text-muted"><i class="icon-location-pin"></i>&nbsp; Palo Alto, CA</small>
                </div>
                <hr class="mx-3 my-0">
                <div class="callout callout-primary m-0 py-3">
                    <div>
                        <strong>جلسه تیم</strong>
                    </div>
                    <small class="text-muted mr-3"><i class="icon-calendar"></i>&nbsp; 4 - 6pm</small>
                    <small class="text-muted"><i class="icon-home"></i>&nbsp; creativeLabs HQ</small>
                </div>
                <hr class="mx-3 my-0">
            </div>
            <div class="tab-pane p-3" id="messages" role="tabpanel">
                <div class="message">

                    </div>
                    <div>
                        <small class="text-muted">Lukasz Holeczek</small>
                        <small class="text-muted float-left mt-1">1:52 PM</small>
                    </div>
                    <div class="text-truncate font-weight-bold">لورم ایپسوم یا طرح‌نما</div>
                    <small class="text-muted">لورم ایپسوم یا طرح‌نما (به انگلیسی: Lorem ipsum) به متنی آزمایشی و بی‌معنی در صنعت...</small>
                </div>
                <hr>
                <div class="message">
                    <div class="py-3 pb-5 mr-3 float-right">
                        <div class="avatar">
                            <img src="img/avatars/7.jpg" class="img-avatar" alt="admin@bootstrapmaster.com">
                            <span class="avatar-status badge-success"></span>
                        </div>
                    </div>
                    <div>
                        <small class="text-muted">Lukasz Holeczek</small>
                        <small class="text-muted float-left mt-1">1:52 PM</small>
                    </div>
                    <div class="text-truncate font-weight-bold">لورم ایپسوم یا طرح‌نما</div>
                    <small class="text-muted">لورم ایپسوم یا طرح‌نما (به انگلیسی: Lorem ipsum) به متنی آزمایشی و بی‌معنی در صنعت...</small>
                </div>
                <hr>
                <div class="message">
                    <div class="py-3 pb-5 mr-3 float-right">
                        <div class="avatar">
                            <img src="img/avatars/7.jpg" class="img-avatar" alt="admin@bootstrapmaster.com">
                            <span class="avatar-status badge-success"></span>
                        </div>
                    </div>
                    <div>
                        <small class="text-muted">Lukasz Holeczek</small>
                        <small class="text-muted float-left mt-1">1:52 PM</small>
                    </div>
                    <div class="text-truncate font-weight-bold">لورم ایپسوم یا طرح‌نما</div>
                    <small class="text-muted">لورم ایپسوم یا طرح‌نما (به انگلیسی: Lorem ipsum) به متنی آزمایشی و بی‌معنی در صنعت...</small>
                </div>
                <hr>
                <div class="message">
                    <div class="py-3 pb-5 mr-3 float-right">
                        <div class="avatar">
                            <img src="img/avatars/7.jpg" class="img-avatar" alt="admin@bootstrapmaster.com">
                            <span class="avatar-status badge-success"></span>
                        </div>
                    </div>
                    <div>
                        <small class="text-muted">Lukasz Holeczek</small>
                        <small class="text-muted float-left mt-1">1:52 PM</small>
                    </div>
                    <div class="text-truncate font-weight-bold">لورم ایپسوم یا طرح‌نما</div>
                    <small class="text-muted">لورم ایپسوم یا طرح‌نما (به انگلیسی: Lorem ipsum) به متنی آزمایشی و بی‌معنی در صنعت...</small>
                </div>
                <hr>
                <div class="message">
                    <div class="py-3 pb-5 mr-3 float-right">
                        <div class="avatar">
                            <img src="img/avatars/7.jpg" class="img-avatar" alt="admin@bootstrapmaster.com">
                            <span class="avatar-status badge-success"></span>
                        </div>
                    </div>
                    <div>
                        <small class="text-muted">Lukasz Holeczek</small>
                        <small class="text-muted float-left mt-1">1:52 PM</small>
                    </div>
                    <div class="text-truncate font-weight-bold">لورم ایپسوم یا طرح‌نما</div>
                    <small class="text-muted">لورم ایپسوم یا طرح‌نما (به انگلیسی: Lorem ipsum) به متنی آزمایشی و بی‌معنی در صنعت...</small>
                </div>
            </div>
            <div class="tab-pane p-3" id="settings" role="tabpanel">
                <h6>تنظیمات</h6>

                <div class="aside-options">
                    <div class="clearfix mt-4">
                        <small><b>گزینه 1</b>
                        </small>
                        <label class="switch switch-text switch-pill switch-success switch-sm float-left">
                            <input type="checkbox" class="switch-input" checked="">
                            <span class="switch-label" data-on="فعال" data-off="قفل"></span>
                            <span class="switch-handle"></span>
                        </label>
                    </div>
                    <div>
                        <small class="text-muted">لورم ایپسوم یا طرح‌نما (به انگلیسی: Lorem ipsum) به متنی آزمایشی و بی‌معنی در صنعت چاپ، صفحه‌آرایی و طراحی گرافیک گفته می‌شود. طراح گرافیک از این متن به عنوان عنصری از ترکیب بندی برای پر کردن صفحه و ارایه اولیه شکل ظاهری و کلی طرح سفارش گرفته شده استفاده می نماید</small>
                    </div>
                </div>

                <div class="aside-options">
                    <div class="clearfix mt-3">
                        <small><b>گزینه 2</b>
                        </small>
                        <label class="switch switch-text switch-pill switch-success switch-sm float-left">
                            <input type="checkbox" class="switch-input">
                            <span class="switch-label" data-on="On" data-off="Off"></span>
                            <span class="switch-handle"></span>
                        </label>
                    </div>
                    <div>
                        <small class="text-muted">لورم ایپسوم یا طرح‌نما (به انگلیسی: Lorem ipsum) به متنی آزمایشی و بی‌معنی در صنعت چاپ، صفحه‌آرایی و طراحی گرافیک گفته می‌شود. طراح گرافیک از این متن به عنوان عنصری از ترکیب بندی برای پر کردن صفحه و ارایه اولیه شکل ظاهری و کلی طرح سفارش گرفته شده استفاده می نماید</small>
                    </div>
                </div>

                <div class="aside-options">
                    <div class="clearfix mt-3">
                        <small><b>گزینه 3</b>
                        </small>
                        <label class="switch switch-text switch-pill switch-success switch-sm float-left">
                            <input type="checkbox" class="switch-input">
                            <span class="switch-label" data-on="فعال" data-off="قفل"></span>
                            <span class="switch-handle"></span>
                        </label>
                    </div>
                </div>

                <div class="aside-options">
                    <div class="clearfix mt-3">
                        <small><b>گزینه 4</b>
                        </small>
                        <label class="switch switch-text switch-pill switch-success switch-sm float-left">
                            <input type="checkbox" class="switch-input" checked="">
                            <span class="switch-label" data-on="فعال" data-off="قفل"></span>
                            <span class="switch-handle"></span>
                        </label>
                    </div>
                </div>

                <hr>
                <h6>میزان استفاده از سیستم </h6>

                <div class="text-uppercase mb-1 mt-4">
                    <small><b>CPU Usage</b>
                    </small>
                </div>
                <div class="progress progress-xs">
                    <div class="progress-bar bg-info" role="progressbar" style="width: 25%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                </div>
                <small class="text-muted">348 Processes. 1/4 Cores.</small>

                <div class="text-uppercase mb-1 mt-2">
                    <small><b>Memory Usage</b>
                    </small>
                </div>
                <div class="progress progress-xs">
                    <div class="progress-bar bg-warning" role="progressbar" style="width: 70%" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100"></div>
                </div>
                <small class="text-muted">11444GB/16384MB</small>

                <div class="text-uppercase mb-1 mt-2">
                    <small><b>SSD 1 Usage</b>
                    </small>
                </div>
                <div class="progress progress-xs">
                    <div class="progress-bar bg-danger" role="progressbar" style="width: 95%" aria-valuenow="95" aria-valuemin="0" aria-valuemax="100"></div>
                </div>
                <small class="text-muted">243GB/256GB</small>

                <div class="text-uppercase mb-1 mt-2">
                    <small><b>SSD 2 Usage</b>
                    </small>
                </div>
                <div class="progress progress-xs">
                    <div class="progress-bar bg-success" role="progressbar" style="width: 10%" aria-valuenow="10" aria-valuemin="0" aria-valuemax="100"></div>
                </div>
                <small class="text-muted">25GB/256GB</small>
            </div>
        </div>
    </aside>


</div>

<footer class="app-footer ltr">
    <a href="http://iracode.com">
    <span class="float-left">Powered by iracode</span></a>
        </span>
</footer>
<script type="text/javascript">
    swal({
        buttons: {
            cancel: true,
            confirm: "Confirm",
            roll: {
                text: "Do a barrell roll!",
                value: "roll",
            },
        },
    });
</script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/8.11.8/sweetalert2.all.js"></script>
@include('sweetalert::alert')


<!-- GenesisUI main scripts -->

<script src="<?php echo url('../js/jquery.js')?>"></script>
<script src="<?php echo url('../js/popper.min.js')?>"></script>
<script src="<?php echo url('../js/bootstrap.min.js')?>"></script>
<script src="<?php echo url('../js/pace.js')?>"></script>


<!-- GenesisUI main scripts -->

<script src="<?php echo url('../js/papp.js')?>"></script>
<script src="<?php echo url('../js/pmain.js')?>"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>



<script src="//cdn.ckeditor.com/4.13.0/full/ckeditor.js"></script>
<script src="/rofil-ckeditor/laravel-ckeditor/adapters/jquery.js"></script>
@yield('script')






<!-- Plugins and scripts required by this views -->

<!-- Custom scripts required by this view -->
<script src="js/views/main.js"></script>

</body>

</html>